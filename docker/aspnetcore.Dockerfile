FROM microsoft/aspnetcore:2.0
RUN mkdir -p /usr/src/web
COPY src/twentyeight-web /usr/src/web
WORKDIR /usr/src/web
ENTRYPOINT ["dotnet", "twentyeightweb.dll"]