#!/bin/bash
set -e
echo "creating user twentyeight and database twentyeight"
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER twentyeight WITH PASSWORD 'twentyeightTEMP';
    CREATE DATABASE twentyeight;
    GRANT ALL PRIVILEGES ON DATABASE twentyeight TO twentyeight;
EOSQL