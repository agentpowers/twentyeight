FROM postgres:9.5
ENV POSTGRES_PASSWORD postgresTEMP

COPY scripts/init_docker_postgres.sh /docker-entrypoint-initdb.d/
