#!/bin/bash

# build client
cd ./src/twentyeightclient
npm run build
# build web
cd ..
cd ..
cd ./src/twentyeightweb
# clean up
rm -rf wwwroot/static/
# copy static files
cp -r ../twentyeightclient/dist/static wwwroot/static/
# copy index to cshtml
cp ../twentyeightclient/dist/index.html Views/Home/Index.cshtml
# build server
dotnet build
# reset to initial root directory
cd ..
cd ..

