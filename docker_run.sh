#!/bin/bash

# build client
cd ./src/twentyeightclient
npm run build
# build web
cd ..
cd ..
cd ./src/twentyeightweb
# clean up
rm -rf wwwroot/static/
# copy static files
cp -r ../twentyeightclient/dist/static wwwroot/static/
# copy index to cshtml
cp ../twentyeightclient/dist/index.html Views/Home/Index.cshtml
# clean up
rm -rf ../../docker/src/twentyeight-web
# publish dotnet core to docker folder
dotnet publish -o ../../docker/src/twentyeight-web
# reset to initial root directory
cd ..
cd ..

#run docker compose
cd docker
docker-compose build
docker-compose up

cd ..

echo 'app is running at http://localhost'

