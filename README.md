Twenty Eight web 28
=====================

Twenty eight is a multiplayer card game.  [Game rules](https://en.wikipedia.org/wiki/Twenty-eight_(card_game)).  
MULTI PLAYER IS WORK IN PROGRESS

Single player works!

Front end 
- [VUE.JS](https://vuejs.org/) - src/twentyeightclient

Back end
- [.NET CORE](https://www.microsoft.com/net/learn/get-started/macos) - src/twentyeightweb
- [postgres](https://www.postgresql.org/)

---
GETTING IT TO RUN
===============

Prerequisite
- .NET CORE
- Npm
- docker & docker compose

Then run 'docker_run.sh' : This will build locally then use docker for running the app
