const rankMapper = function (rank) {
  switch (rank) {
    case 'Jack': return 'J'
    case 'Nine': return '9'
    case 'Ace': return 'A'
    case 'Ten': return '10'
    case 'King': return 'K'
    case 'Queen': return 'Q'
    case 'Eight': return '8'
    case 'Seven': return '7'
    case 'Six': return '6'
    default: return ''
  }
}
export { rankMapper }
