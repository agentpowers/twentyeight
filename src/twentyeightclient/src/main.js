import Vue from 'vue'
import App from './App'
import store from './store/store'

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  render: h => h(App),
  created: function () {
    //  init
    store.dispatch('init')
  }
})
