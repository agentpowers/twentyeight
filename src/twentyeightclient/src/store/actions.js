import * as services from '../services'
import store from './store'

const initStoreAndStartGame = (commit, state, gameId) => {
  commit('applyGameId', gameId)
  services.initWs(store)
  services.sendWsAction('startGame', {gameId: state.game.gameId, userId: state.game.playerId})
}

const joinGame = (state) =>
  services.post('api/room/join', {userId: state.game.playerId, gameId: state.game.gameId, userName: 'PLAYER'})

const actions = {
  init: function ({commit, state}) {
    services.get(`api/room?userId=${state.game.playerId}`)
      .then(function (data) {
        const gameId = data.id
        if (data.players) {
          initStoreAndStartGame(commit, state, gameId)
        } else {
          joinGame(state).then(function (data) {
            initStoreAndStartGame(commit, state, gameId)
          })
        }
      }, function (request) {
        if (request.status === 404) {
          services.post('api/room/add', {userId: state.game.playerId, gameId: state.game.gameId})
            .then(function (data) {
              const gameId = data
              joinGame(state).then(function (data) {
                initStoreAndStartGame(commit, state, gameId)
              })
            })
        }
      })
  },
  startGame: function ({commit, state}) {
    /*
    services.post('api/startGame', {gameId: state.game.gameId, userId: state.game.playerId})
      .then(function (data) {
      //  debugger
        if (data.Error) {
          window.alert(data.Error)
          return
        }
        commit('applyStartGame', data.Ok)
      })
      */
    services.sendWsAction('startGame', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  getGame: function ({commit, state}) {
    /*
    services.post('api/game', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyGame', data.Ok)
    })
    */
    services.sendWsAction('game', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  restartRound: function ({commit, state}) {
    /*
    services.post('api/restartRound', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyRoundRestart', data.Ok)
    })
    */
    services.sendWsAction('restartRound', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  moveToNextRound: function ({commit, state}) {
    /*
    services.post('api/moveToNextRound', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyStartedNewRound', data.Ok)
    })
    */
    services.sendWsAction('moveToNextRound', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  playCard: function ({commit, state}, card) {
    /*
    services.post('api/playCard', {rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyPlayed', data.Ok.Played)
    })
    */
    services.sendWsAction('playCard', {rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
  },
  pass: function ({commit, state}) {
    /*
    services.post('api/pass', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyPassPosted', data.Ok.PassPosted)
    })
    */
    services.sendWsAction('pass', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  thani: function ({commit, state}) {
    /*
    services.post('api/thani', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyThaniPosted', data.Ok.ThaniPosted)
    })
    */
    services.sendWsAction('thani', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  openThurupu: function ({commit, state}) {
    /*
    services.post('api/openThurupu', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyOpenThurupuPosted', data.Ok.OpenThurupuPosted)
    })
    */
    services.sendWsAction('openThurupu', {gameId: state.game.gameId, userId: state.game.playerId})
  },
  bid: function ({commit, state}, {bid, card}) {
    /*
    services.post('api/bid', {bid: bid, rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyBidPosted', {bid: data.Ok.BidPosted, trump: card})
    })
    */
    services.sendWsAction('bid', {bid: bid, rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
  },
  addGame: function ({commit, state}) {
    services.post('api/room/add', {userId: state.game.playerId})
      .then(function (data) {
      //  debugger
        if (data.Error) {

        }
      })
  },
  getNext: function ({commit, state}) {
    /*
    services.post('api/getNext', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
      } else if (data.Ok.Played) {
        commit('applyPlayed', data.Ok.Played)
      } else if (data.Ok.OpenThurupuPosted) {
        commit('applyOpenThurupuPosted', data.Ok.OpenThurupuPosted)
      } else if (data.Ok.PassPosted) {
        commit('applyPassPosted', data.Ok.PassPosted)
      } else if (data.Ok.BidPosted) {
        commit('applyBidPosted', {bid: data.Ok.BidPosted})
      } else if (data.Ok === 'EndedGame') {
        window.alert('game ended')
      } else if (data.StartedNewRound) {
        commit('applyStartedNewRound', data.StartedNewRound)
      } else if (data.MovedToNextRound) {
        commit('applyMovedToNextRound', data.MovedToNextRound)
      } else if (data.RestartedRound) {
        commit('applyRestartedRound', data.RestartedRound)
      } else {
        throw new Error('unable to process result from getNext')
      }
    })
    */
    services.sendWsAction('getNext', {gameId: state.game.gameId, userId: state.game.playerId})
  }
}
export default actions
