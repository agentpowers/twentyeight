import { findIndex } from 'lodash'

class Hand {
  constructor () {
    this.type = null
    this.suit = null
    this.rank = null
  }
  static hydrate (type, data, hand) {
    hand.type = type
    hand.suit = data.suit
    hand.rank = data.rank
  }
  static applyPlayed (card, type, hand) {
    hand.type = type
    hand.suit = card.suit
    hand.rank = card.rank
  }
  static applyType (type, hand) {
    hand.type = type
  }
}
class Player {
  constructor (isUser) {
    this.ai = false
    this.isUser = isUser || false
    this.kunukus = 0
    this.name = ''
    this.playerId = ''
    this.ready = false
    this.teamId = 0
    this.askedForThurupuPlayIndex = 0
    this.hand = []
    this.message = ''
  }
  static hydrate (data, player) {
    player.ai = data.ai
    player.kunukus = data.kunukus
    player.name = data.name
    player.playerId = data.playerId
    player.ready = data.ready
    player.teamId = data.teamId
    player.message = ''
    if (player.isUser && data.hand.length > 0) {
      for (var index = 0; index < data.hand.length; index++) {
        var val = data.hand[index]
        if (!player.hand[index]) {
          player.hand.push(new Hand())
        }
        Hand.hydrate('Open', val, player.hand[index])
      }
    }
  }
  static applyMessage (msg, player) {
    player.message = msg
  }
  static findHand (card, player) {
    for (var i = 0; i < player.hand.length; i++) {
      var element = player.hand[i]
      if (card.suit === element.suit && card.rank === element.rank) {
        return element
      }
    }
  }
  static applyPlayedHand (card, player) {
    if (player.isUser) {
      Hand.applyPlayed(card, 'Hidden', Player.findHand(card, player))
    }
  }
  static applyTrumpCard (card, player) {
    if (player.isUser) {
      var hand = Player.findHand(card, player)
      if (hand) {
        Hand.applyType('Trump', hand)
      }
    }
  }
  static applyOpenedTrumpCard (card, player) {
    if (player.isUser) {
      var hand = Player.findHand(card, player)
      if (hand) {
        Hand.applyType('Open', hand)
      }
    }
  }
  static askedForThurupuPlayIndex (askedForThurupuPlayIndex, player) {
    player.askedForThurupuPlayIndex = askedForThurupuPlayIndex
  }
}
class Bid {
  constructor () {
    this.status = null
    this.bidderId = null
    this.value = null
    this.openedPlayerId = null
    this.openedSetId = null
    this.openedTrickId = null
    this.openedAt = null
    this.passCount = 0
    this.thurupu = {
      suit: null,
      rank: null
    }
  }
  static reset (bid) {
    bid.status = null
    bid.bidderId = null
    bid.value = null
    bid.openedPlayerId = null
    bid.openedSetId = null
    bid.openedTrickId = null
    bid.openedAt = null
    bid.thurupu.suit = null
    bid.thurupu.rank = null
    bid.passCount = 0
  }
  static hydrate (data, bid) {
    Bid.reset(bid)
    if (data === 'WaitingForBid') {
      bid.status = 'WaitingForBid'
    } else if (data.ProcessingBid) {
      bid.status = 'ProcessingBid'
      bid.bidderId = data.ProcessingBid.bidderId
      bid.value = data.ProcessingBid.value
    } else if (data.ThaniBid) {
      bid.status = 'ThaniBid'
      bid.bidderId = data.ThaniBid
    } else if (data.Bid) {
      bid.status = 'Bid'
      bid.bidderId = data.Bid.bidderId
      bid.value = data.Bid.value
    } else if (data.OpenedBid) {
      bid.status = 'OpenedBid'
      bid.bidderId = data.OpenedBid[0].bidderId
      bid.value = data.OpenedBid[0].value
      bid.openedPlayerId = data.OpenedBid[1].openedPlayerId
      bid.openedSetId = data.OpenedBid[1].openedSetId
      bid.openedTrickId = data.OpenedBid[1].openedTrickId
      bid.openedAt = data.OpenedBid[1].openedAt
      bid.thurupu.suit = data.OpenedBid[0].thurupu.suit
      bid.thurupu.rank = data.OpenedBid[0].thurupu.rank
    } else {
      throw new Error('unable to hydrate bid')
    }
  }
  static apply (data, type, bid) {
    switch (type) {
      case 'WaitingForBid':
        bid.status = 'WaitingForBid'
        break
      case 'ProcessingBid':
        bid.status = 'ProcessingBid'
        bid.bidderId = data.bidderId
        bid.value = data.value
        bid.passCount = 0
        break
      case 'Pass':
        bid.passCount++
        //  if 5 passed then change from ProcessingBid to Bid
        if (bid.passCount === 5) {
          bid.status = 'Bid'
        }
        break
      case 'ThaniBid':
        bid.status = 'ThaniBid'
        bid.bidderId = data.bidderId
        break
      case 'Bid':
        bid.status = 'Bid'
        bid.bidderId = data.bidderId
        bid.value = data.value
        break
      case 'OpenedBid':
        bid.status = 'OpenedBid'
        bid.openedPlayerId = data.openedPlayerId
        bid.openedSetId = data.openedSetId
        bid.openedTrickId = data.openedTrickId
        bid.openedAt = data.openedAt
        bid.thurupu.suit = data.thurupu.suit
        bid.thurupu.rank = data.thurupu.rank
        break
      default:
        throw new Error('unknown bid status')
    }
  }
}
class Round {
  constructor (playerId) {
    this.playerId = playerId
    this.status = null
    this.bid = new Bid()
    this.currentPlayerId = null
    this.playerIndexMap = {}
    this.currentTrickIndex = 0
    this.currentSetIndex = 0
    this.id = ''
    this.players = [new Player(true), new Player(), new Player(), new Player(), new Player(), new Player()]
    this.sets = []
    this.startingPlayerId = null
    this.startingPlayerIndex = 0
    this.message = {
      text: '',
      type: ''
    }
  }
  static hydrate (data, round) {
    round.startingPlayerIndex = data.startingPlayerIndex
    round.startingPlayerId = data.startingPlayerId
    //  reverse sets and tricks
    for (var i = 0; i < data.sets.length; i++) {
      var set = data.sets[i]
      set.tricks.reverse()
    }
    round.message = {
      text: '',
      type: ''
    }
    round.sets = data.sets.reverse()
    round.id = data.id
    Bid.hydrate(data.bid, round.bid)
    // round.bid.hydrate(data.bid)
    round.currentTrickIndex = data.currentTrickIndex
    round.currentSetIndex = data.currentSetIndex
    round.currentPlayerId = data.currentPlayerId
    round.status = data.status
    round.playerIndexMap = {}
    var userPlayerIndex = findIndex(data.players, {playerId: round.playerId})
    for (var index = 0; index < data.players.length; index++) {
      var playerIndex = (userPlayerIndex + index) % 6
      Player.hydrate(data.players[index], round.players[playerIndex])
      //  set current player index
      round.playerIndexMap[data.players[playerIndex].playerId] = index
    }
  }
  static isThaniBid (round) {
    return round.bid.status === 'ThaniBid'
  }
  static biddingCompleted (round) {
    return round.bid.status === 'Bid' || round.bid.status === 'ThaniBid' || round.bid.status === 'OpenedBid'
  }
  static playCount (round) {
    return (6 * round.currentSetIndex) + round.currentTrickIndex
  }
  static currentPlayerIndex (round) {
    return round.playerIndexMap[round.currentPlayerId]
  }
  static isUserCurrentPlayer (round) {
    return round.playerId === round.currentPlayerId
  }
  static setPlayerMessage (msg, playerId, round) {
    Player.applyMessage(msg, round.players[round.playerIndexMap[playerId]])
  }
  static moveToNextPlayer (userId, reason, currentPlayerId, round) {
    //  window.console.log('current player ' + reason + ', moving to next player:' + currentPlayerId)
    round.currentPlayerId = currentPlayerId
  }
  static applyMessage (message, type, round) {
    round.message.text = message
    round.message.type = type
  }
  static applyPlayed (userId, set, type, round) {
    const currentPlayerIndex = Round.currentPlayerIndex(round)
    Player.applyPlayedHand(set.trick.playedCard, round.players[currentPlayerIndex])
    Round.moveToNextPlayer(userId, type + ' card', set.currentPlayerId, round)
    round.currentTrickIndex = set.currentTrickIndex
    if (((round.sets.length - 1) < set.currentSetIndex) || round.sets.length === 0) {
      round.currentSetIndex = set.currentSetIndex
      round.sets.push({
        completedAt: set.completedAt,
        currentPlayerId: set.currentPlayerId,
        cutBy: set.cutBy,
        isCut: set.isCut,
        setId: set.setId,
        winner: set.winner,
        tricks: []
      })
    } else {
      var s = round.sets[round.currentSetIndex]
      s.completedAt = set.completedAt
      s.cutBy = set.cutBy
      s.isCut = set.isCut
      s.setId = set.setId
      s.winner = set.winner
    }
    if (round.currentSetIndex > 0 && round.currentTrickIndex === 0) {
      //  set just ended
      round.sets[round.currentSetIndex - 1].tricks.push(set.trick)
      Round.applyMessage('🎉 ' + round.players[round.playerIndexMap[set.winner]].name + ' is the winner 🎉', '', round)
    } else {
      round.sets[round.currentSetIndex].tricks.push(set.trick)
      Round.applyMessage('', '', round)
    }
    if (Round.playCount(round) === 36) {
      round.status = 'Ended'
    }
  }
  static applyOpenThurupuPosted (userId, thurupu, round) {
    Round.applyMessage('Opened Thurupu', '', round)
    Bid.apply({ openedPlayerId: userId, thurupu }, 'OpenedBid', round.bid)
    if (round.bid.bidderId === round.playerId && thurupu) {
      Player.applyOpenedTrumpCard(thurupu, round.players[round.playerIndexMap[round.playerId]])
    }
  }
  static applyPassPosted (userId, currentPlayerId, round) {
    Round.setPlayerMessage('Pass', userId, round)
    Round.applyMessage('', '', round)
    Round.moveToNextPlayer(userId, 'posted pass', currentPlayerId, round)
    Bid.apply({}, 'Pass', round.bid)
  }
  static applyThaniPosted (userId, currentPlayerId, round) {
    Round.setPlayerMessage('Thani', userId, round)
    Round.applyMessage('', '', round)
    Round.moveToNextPlayer(userId, 'posted thani', currentPlayerId, round)
    Bid.apply({ bidderId: userId }, 'ThaniBid', round.bid)
  }
  static applyBidPosted (userId, bid, currentPlayerId, thurupu, round) {
    Round.setPlayerMessage('Bid', userId, round)
    Round.applyMessage('', '', round)
    Round.moveToNextPlayer(userId, 'posted bid', currentPlayerId, round)
    Bid.apply(bid, 'ProcessingBid', round.bid)
    if (round.bid.bidderId === round.playerId && thurupu) {
      Player.applyTrumpCard(thurupu, round.players[round.playerIndexMap[round.playerId]])
    }
  }
}
class Game {
  constructor (playerId, gameId) {
    this.playerId = playerId
    this.gameId = gameId
    this.team1 = null
    this.team2 = null
    this.round = new Round(this.playerId)
  }

  static hydrate (data, game) {
    Round.hydrate(data.round, game.round)
  }
}
export {
  Game,
  Round,
  Player,
  Bid
}
