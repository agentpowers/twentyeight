import { Game, Round } from './game'

const applyGame = function (state, data) {
  Game.hydrate(data, state.game)
}
const applyRound = function (state, data) {
  var event = data.RestartedRound || data.MovedToNextRound || data.StartedNewRound
  Round.hydrate(event ? event[1] : data, state.game.round)
}
const applyPlayed = function (state, data) {
  Round.applyPlayed(data[0], data[1], '', state.game.round)
}
const applyOpenThurupuPosted = function (state, data) {
  Round.applyOpenThurupuPosted(data[0], data[1], state.game.round)
}
const applyPassPosted = function (state, data) {
  Round.applyPassPosted(data[0], data[1], state.game.round)
}
const applyThaniPosted = function (state, data) {
  Round.applyThaniPosted(data[0], data[1], state.game.round)
}
const applyBidPosted = function (state, data) {
  Round.applyBidPosted(data.bid[0], data.bid[1], data.bid[2], data.trump, state.game.round)
}
const applyMessage = function (state, data) {
  Round.applyMessage(data.message, data.type, state.game.round)
}
const applyGameId = function (state, gameId) {
  state.game.gameId = gameId
}
const mutations = {
  applyGame,
  applyStartGame: applyGame,
  applyRoundRestart: applyRound,
  applyMovedToNextRound: applyRound,
  applyStartedNewRound: applyRound,
  applyPlayed,
  applyOpenThurupuPosted,
  applyPassPosted,
  applyThaniPosted,
  applyBidPosted,
  applyMessage,
  applyGameId
}
export default mutations
