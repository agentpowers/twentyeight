import Vue from 'vue'
import Vuex from 'vuex'
import { Game } from './game'
import mutations from './mutations'
import actions from './actions'

Vue.use(Vuex)
const gameId = 'c46f8f2d-54d1-4c05-932c-bc9b864fac60'
const playerId = 'c46f8f2d-54d1-4c05-932c-bc9b864fac60'
var store = new Vuex.Store({
  state: {
    game: new Game(gameId, playerId)
  },
  getters: {
  },
  mutations,
  actions
})
export default store
