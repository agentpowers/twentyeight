const commits = {
  startGame: function (data, {commit}) {
    //  debugger
    if (data.error) {
      window.alert(data.error)
      return
    }
    commit('applyStartGame', data.ok)
  },
  game: function (data, {commit}) {
    if (data.error) {
      window.alert(data.error)
      return
    }
    commit('applyGame', data.ok)
  },
  restartRound: function (data, {commit}) {
    //  debugger
    if (data.error) {
      window.alert(data.error)
      return
    }
    commit('applyRoundRestart', data.ok)
  },
  moveToNextRound: function (data, {commit}) {
    //  debugger
    if (data.error) {
      window.alert(data.error)
      return
    }
    commit('applyStartedNewRound', data.ok)
  },
  playCard: function (data, {commit}) {
    //  debugger
    if (data.error) {
      commit('applyMessage', {message: data.error, type: 'Error'})
      return
    }
    commit('applyPlayed', data.ok.Played)
  },
  pass: function (data, {commit}) {
    //  debugger
    if (data.error) {
      commit('applyMessage', {message: data.error, type: 'Error'})
      return
    }
    commit('applyPassPosted', data.ok.PassPosted)
  },
  thani: function (data, {commit}) {
    //  debugger
    if (data.error) {
      commit('applyMessage', {message: data.error, type: 'Error'})
      return
    }
    commit('applyThaniPosted', data.ok.ThaniPosted)
  },
  openThurupu: function (data, {commit}) {
    //  debugger
    if (data.error) {
      commit('applyMessage', {message: data.error, type: 'Error'})
      return
    }
    commit('applyOpenThurupuPosted', data.ok.OpenThurupuPosted)
  },
  bid: function (data, {commit}) {
    //  debugger
    if (data.error) {
      commit('applyMessage', {message: data.error, type: 'Error'})
      return
    }
    commit('applyBidPosted', {
      bid: data.ok.BidPosted,
      trump: {rank: getParameterByName('rank', data.action.parameters), suit: getParameterByName('suit', data.action.parameters)}
    })
  },
  getNext: function (data, {commit}) {
    //  debugger
    if (data.error) {
      window.alert(data.error)
    } else if (data.ok.Played) {
      commit('applyPlayed', data.ok.Played)
    } else if (data.ok.OpenThurupuPosted) {
      commit('applyOpenThurupuPosted', data.ok.OpenThurupuPosted)
    } else if (data.ok.PassPosted) {
      commit('applyPassPosted', data.ok.PassPosted)
    } else if (data.ok.BidPosted) {
      commit('applyBidPosted', {bid: data.ok.BidPosted})
    } else if (data.ok === 'EndedGame') {
      window.alert('game ended')
    } else if (data.StartedNewRound) {
      commit('applyStartedNewRound', data.StartedNewRound)
    } else if (data.MovedToNextRound) {
      commit('applyMovedToNextRound', data.MovedToNextRound)
    } else if (data.RestartedRound) {
      commit('applyRestartedRound', data.RestartedRound)
    } else {
      throw new Error('unable to process result from getNext')
    }
  }
}
var socket = null
var socketReady = false
var socketWait = 0
const parseResult = function (data) {
  /* if (!result.error) {
    result.ok = JSON.parse(result.ok)
  }
  return result
  */
  // changing Ok and Error to camel Case
  const parsedResult = JSON.parse(data.result)
  return { ok: parsedResult.Ok, error: parsedResult.Error }
}
const initWs = function (store) {
  // Create WebSocket connection.
  socket = new WebSocket('ws://' + window.location.host + '/api/ws?userId=' + store.state.game.playerId)

  // Listen for messages
  socket.addEventListener('message', function (event) {
    //  console.log('Message from server : ' + event.data)
    var eventData = JSON.parse(event.data)
    if (eventData.messageType === 3 && eventData.data.action) {
      commits[eventData.data.action.actionType](parseResult(eventData.data), store)
    } else {
      console.log('message : ' + event.data)
    }
  })

  // Connection opened
  socket.addEventListener('open', function (event) {
    //  socket.send('Hello Server!')
    socketReady = true
  })

  // Connection closed
  socket.addEventListener('close', function (event) {
    socketReady = false
  })
}
const getParameterByName = function (name, url) {
  // debugger
  return url.substr(url.indexOf(name + '=')).split('&')[0].split('=')[1]
}
const param = function (object) {
  var encodedString = ''
  for (var prop in object) {
    if (object.hasOwnProperty(prop)) {
      if (encodedString.length > 0) {
        encodedString += '&'
      }
      encodedString += encodeURI(prop + '=' + object[prop])
    }
  }
  return encodedString
}
const sendWsAction = function (type, data) {
  if (socketWait > 5) {
    console.log('socket connection error')
    return
  }
  if (!socketReady) {
    socketWait++
    window.setTimeout(function () {
      sendWsAction(type, data)
    }, 500)
    return
  }
  socketWait = 0
  socket.send(JSON.stringify({ActionType: type, Parameters: data}))
}
const post = function (url, data) {
  return new window.Promise(
    function (resolve, reject) {
      var request = new window.XMLHttpRequest()
      request.open('POST', url, true)
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
      request.send(param(data))
      request.onload = function () {
        if (request.status === 200) {
          resolve(window.JSON.parse(request.responseText))
        }
        reject(request)
      }
    }
  )
}

const get = function (url) {
  return new window.Promise(
    function (resolve, reject) {
      var request = new window.XMLHttpRequest()
      request.open('GET', url, true)
      request.send()
      request.onload = function () {
        if (request.status === 200) {
          resolve(window.JSON.parse(request.responseText))
        }
        reject(request)
      }
    }
  )
}

export { post, get, initWs, sendWsAction }
