webpackJsonp([1],{

/***/ "+zz6":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "4s90":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_minicard_vue__ = __webpack_require__("MVFa");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_7c298371_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_minicard_vue__ = __webpack_require__("KSsP");
function injectStyle (ssrContext) {
  __webpack_require__("VcOW")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7c298371"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_minicard_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_7c298371_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_minicard_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "5YWN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{staticClass:"suit",attrs:{"suit-type":_vm.suit}},[_vm._v(_vm._s(_vm.suitText))])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "5xdz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'suit',
  props: ['suit'],
  computed: {
    'suitText': function suitText() {
      if (this.suit === 'Heart') {
        return '♥';
      }
      if (this.suit === 'Clubs') {
        return '♣';
      }
      if (this.suit === 'Dice') {
        return '♦';
      }
      if (this.suit === 'Spade') {
        return '♠';
      }
      return '';
    }
  }
});

/***/ }),

/***/ "KSsP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"card",attrs:{"card-type":_vm.card.type}},[_c('div',{staticClass:"mini-container"},[_c('span',{staticClass:"number"},[_vm._v(_vm._s(_vm.number))])]),_vm._v(" "),_c('suit',{staticClass:"suit-icon",attrs:{"suit":_vm.card.suit}})],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "L/hj":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return rankMapper; });
var rankMapper = function rankMapper(rank) {
  switch (rank) {
    case 'Jack':
      return 'J';
    case 'Nine':
      return '9';
    case 'Ace':
      return 'A';
    case 'Ten':
      return '10';
    case 'King':
      return 'K';
    case 'Queen':
      return 'Q';
    case 'Eight':
      return '8';
    case 'Seven':
      return '7';
    case 'Six':
      return '6';
    default:
      return '';
  }
};


/***/ }),

/***/ "M93x":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__ = __webpack_require__("xJD8");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_e160879c_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__ = __webpack_require__("hlbm");
function injectStyle (ssrContext) {
  __webpack_require__("xCgB")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_e160879c_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "MV0V":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("L/hj");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__suit__ = __webpack_require__("Wbmn");
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'card',
  props: ['card'],
  components: {
    Suit: __WEBPACK_IMPORTED_MODULE_1__suit__["a" /* default */]
  },
  methods: {
    'clicked': function clicked() {
      this.$emit('clicked');
    }
  },
  computed: {
    'number': function number() {
      return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* rankMapper */])(this.card.rank);
    }
  }
});

/***/ }),

/***/ "MVFa":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils__ = __webpack_require__("L/hj");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__suit__ = __webpack_require__("Wbmn");
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'minicard',
  props: ['card'],
  components: {
    Suit: __WEBPACK_IMPORTED_MODULE_1__suit__["a" /* default */]
  },
  computed: {
    'number': function number() {
      return Object(__WEBPACK_IMPORTED_MODULE_0__utils__["a" /* rankMapper */])(this.card.rank);
    }
  }
});

/***/ }),

/***/ "NHnr":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("7+uW");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App__ = __webpack_require__("M93x");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_store__ = __webpack_require__("wtEF");




/* eslint-disable no-new */
new __WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */]({
  store: __WEBPACK_IMPORTED_MODULE_2__store_store__["a" /* default */],
  el: '#app',
  render: function render(h) {
    return h(__WEBPACK_IMPORTED_MODULE_1__App__["a" /* default */]);
  },
  created: function created() {
    //  init
    __WEBPACK_IMPORTED_MODULE_2__store_store__["a" /* default */].dispatch('init');
  }
});

/***/ }),

/***/ "RcUN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__card__ = __webpack_require__("UCfo");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__player__ = __webpack_require__("aN8A");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vuex__ = __webpack_require__("NYxO");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_game__ = __webpack_require__("wOfB");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






var state = {
  bidMessage: '',
  bidValue: null
};

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'game',
  components: {
    Card: __WEBPACK_IMPORTED_MODULE_0__card__["a" /* default */],
    Player: __WEBPACK_IMPORTED_MODULE_1__player__["a" /* default */]
  },
  data: function data() {
    return state;
  },
  methods: {
    restartRound: function restartRound() {
      this.$store.dispatch('restartRound');
    },
    moveToNextRound: function moveToNextRound() {
      this.$store.dispatch('moveToNextRound');
    },
    getNext: function getNext() {
      this.$store.dispatch('getNext');
    },
    startGame: function startGame() {
      this.$store.dispatch('startGame');
    },
    pass: function pass() {
      this.$store.dispatch('pass');
    },
    thani: function thani() {
      this.$store.dispatch('thani');
    },
    bid: function bid(card) {
      if (!this.bidValue) {
        this.bidMessage = 'Choose a Bid Value First';
      } else {
        this.$store.dispatch('bid', { card: card, bid: this.bidValue });
        this.bidValue = null;
        this.bidMessage = '';
      }
    },
    openTrump: function openTrump() {
      this.$store.dispatch('openThurupu');
    },
    setBidValue: function setBidValue(bidValue) {
      this.bidValue = bidValue;
      this.bidMessage = 'Bid is ' + bidValue + ', now pick a Trump Card';
    },
    playCard: function playCard(card) {
      if (this.isUserCurrentPlayer) {
        if (this.playStarted) {
          this.$store.dispatch('playCard', card);
        } else {
          this.bid(card);
        }
      }
    }
  },
  computed: Object(__WEBPACK_IMPORTED_MODULE_2_vuex__["b" /* mapState */])({
    'isUserCurrentPlayer': function isUserCurrentPlayer(state) {
      return __WEBPACK_IMPORTED_MODULE_3__store_game__["b" /* Round */].isUserCurrentPlayer(state.game.round);
    },
    'playStarted': function playStarted(state) {
      return __WEBPACK_IMPORTED_MODULE_3__store_game__["b" /* Round */].biddingCompleted(state.game.round);
    },
    'isThaniBid': function isThaniBid(state) {
      return __WEBPACK_IMPORTED_MODULE_3__store_game__["b" /* Round */].isThaniBid(state.game.round);
    },
    'roundMessage': function roundMessage(state) {
      return state.game.round.message;
    },
    'roundStatus': function roundStatus(state) {
      return state.game.round.status;
    },
    'isEmptyGame': function isEmptyGame(state) {
      return state.game.round.id === '00000000-0000-0000-0000-000000000000';
    },
    'player1': function player1(state) {
      return state.game.round.players[0];
    },
    'player2': function player2(state) {
      return state.game.round.players[1];
    },
    'player3': function player3(state) {
      return state.game.round.players[2];
    },
    'player4': function player4(state) {
      return state.game.round.players[3];
    },
    'player5': function player5(state) {
      return state.game.round.players[4];
    },
    'player6': function player6(state) {
      return state.game.round.players[5];
    },
    'showBid': function showBid() {
      return !this.playStarted && this.isUserCurrentPlayer;
    },
    'showOpenTrump': function showOpenTrump(state) {
      return this.playStarted && !this.isThaniBid && this.isUserCurrentPlayer && !state.game.round.bid.openedPlayerId;
    },
    'hand': function hand() {
      return this.player1.hand.filter(function (card) {
        return card.type !== 'Hidden';
      });
    }
  })
});

/***/ }),

/***/ "UCfo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_card_vue__ = __webpack_require__("MV0V");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_8c82a1cc_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_card_vue__ = __webpack_require__("dJ28");
function injectStyle (ssrContext) {
  __webpack_require__("rPrZ")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8c82a1cc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_card_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_8c82a1cc_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_card_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "VcOW":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Wbmn":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_suit_vue__ = __webpack_require__("5xdz");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4b53f897_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_suit_vue__ = __webpack_require__("5YWN");
function injectStyle (ssrContext) {
  __webpack_require__("zmmk")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4b53f897"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_suit_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4b53f897_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_suit_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "X8Nu":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"player",class:{ 'current-player': _vm.isCurrentPlayer },attrs:{"player-number":_vm.playerNumber}},[_c('div',{staticClass:"details",attrs:{"player-number":_vm.playerNumber}},[(_vm.isDealer)?_c('div',{staticClass:"dealer",attrs:{"title":"Dealer"}},[_vm._v("D")]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"title",attrs:{"title":"Player Name"}},[_vm._v(_vm._s(_vm.player.name))]),_vm._v(" "),(_vm.showBidInfo)?_c('div',{staticClass:"bid"},[_c('span',[_vm._v(_vm._s(_vm.bidText))]),_vm._v(" "),_c('suit',{attrs:{"suit":_vm.bidSuit}})],1):_vm._e(),_vm._v(" "),(_vm.player.kunukus > 0)?_c('div',{staticClass:"kunuku",attrs:{"title":"# of kunuku"}},[_vm._v(_vm._s(_vm.player.kunukus))]):_vm._e()]),_vm._v(" "),_c('div',{staticClass:"played"},[(!_vm.playStarted)?_c('div',{staticClass:"message"},[_vm._v(_vm._s(_vm.player.message))]):_vm._e(),_vm._v(" "),_vm._l((_vm.tricks),function(trick){return _c('minicard',{key:trick.PlayerId,attrs:{"card":trick.playedCard}})})],2)])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "aN8A":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_player_vue__ = __webpack_require__("jSyP");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4c282c2a_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_player_vue__ = __webpack_require__("X8Nu");
function injectStyle (ssrContext) {
  __webpack_require__("+zz6")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4c282c2a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_player_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4c282c2a_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_player_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "d2rk":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "dJ28":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.card.type === 'Closed')?_c('div',{staticClass:"card card-background"}):_c('div',{staticClass:"card",attrs:{"card-type":_vm.card.type},on:{"click":_vm.clicked}},[_c('div',{staticClass:"mini-container"},[_c('span',{staticClass:"number"},[_vm._v(_vm._s(_vm.number))]),_vm._v(" "),_c('suit',{attrs:{"suit":_vm.card.suit}})],1),_vm._v(" "),_c('suit',{staticClass:"suit-icon",attrs:{"suit":_vm.card.suit}})],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "dwS6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',[_vm._m(0),_vm._v(" "),_c('div',{staticClass:"container"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"box3"}),_vm._v(" "),_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player4,"player-number":"4"}})],1),_vm._v(" "),_c('div',{staticClass:"box3"})]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player5,"player-number":"5"}})],1),_vm._v(" "),_c('div',{staticClass:"box3"},[(_vm.showBid)?_c('div',{staticClass:"bid"},[_c('div',{staticClass:"bid-pass clickable",on:{"click":_vm.pass}},[_vm._v("Pass")]),_vm._v(" "),_c('span',[_vm._v(" | ")]),_vm._v(" "),_c('div',{staticClass:"bid-thani clickable",on:{"click":_vm.thani}},[_vm._v("Thani")]),_vm._v(" "),_c('div',{staticClass:"bid-values"},[_vm._l((13),function(n){return _c('div',{key:n,staticClass:"bid-value clickable",class:{ 'bid-selected-value' : (n + 15) === _vm.bidValue},on:{"click":function($event){_vm.setBidValue(n + 15)}}},[_vm._v(_vm._s(n + 15))])}),_vm._v(" "),_c('div',{staticClass:"bid-message"},[_vm._v(_vm._s(_vm.bidMessage))])],2)]):_vm._e()]),_vm._v(" "),_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player3,"player-number":"3"}})],1)]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player6,"player-number":"6"}})],1),_vm._v(" "),_c('div',{staticClass:"box3"},[(_vm.showOpenTrump)?_c('div',{staticClass:"open-trump-card clickable",on:{"click":_vm.openTrump}},[_vm._v("Open Trump Card")]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"message",attrs:{"message-type":_vm.roundMessage.type}},[_vm._v(_vm._s(_vm.roundMessage.text))])]),_vm._v(" "),_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player2,"player-number":"2"}})],1)]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"box3"}),_vm._v(" "),_c('div',{staticClass:"box3"},[_c('Player',{attrs:{"player":_vm.player1,"player-number":"1"}})],1),_vm._v(" "),_c('div',{staticClass:"box3"},[(_vm.isEmptyGame === false)?_c('div',[(_vm.roundStatus !== 'Ended')?_c('a',{staticClass:"btn",attrs:{"href":"#","title":"Next"},on:{"click":_vm.getNext}},[_vm._v("MOVE NEXT ⏩")]):_vm._e(),_vm._v(" "),_c('span',{staticClass:"btn"},[_vm._v(" | ")]),_vm._v(" "),_c('a',{staticClass:"btn",attrs:{"href":"#","title":"Restart Round"},on:{"click":_vm.restartRound}},[_vm._v("RESTART ROUND 🔄")]),_vm._v(" "),(_vm.roundStatus === 'Ended')?_c('a',{staticClass:"btn",attrs:{"href":"#","title":"Move To Next Round"},on:{"click":_vm.moveToNextRound}},[_vm._v("TO NEXT ROUND⏩⚪")]):_vm._e()]):_vm._e(),_vm._v(" "),(_vm.isEmptyGame)?_c('div',[_c('a',{staticClass:"btn",attrs:{"href":"#","title":"Start Game"},on:{"click":_vm.startGame}},[_vm._v("NEW GAME 🆕")])]):_vm._e()])]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"box"},[_c('div',{staticClass:"cards"},_vm._l((_vm.hand),function(card){return _c('card',{key:card.rank + card.suit,attrs:{"card":card},on:{"clicked":function($event){_vm.playCard(card)}}})}))])])])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"header"},[_c('div',{staticClass:"title"},[_vm._v("Twenty Eight ")]),_vm._v(" "),_c('div',[_c('a',{attrs:{"target":"_blank","href":"https://en.wikipedia.org/wiki/Twenty-eight_(card_game)"}},[_vm._v("Game rules")]),_vm._v(" "),_c('span',[_vm._v(" | ")]),_vm._v(" "),_c('span',[_vm._v("Hint: use \"move next\" for auto play next")])])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "fU1Q":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return post; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return get; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return initWs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return sendWsAction; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify__ = __webpack_require__("mvHQ");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify__);

var commits = {
  startGame: function startGame(data, _ref) {
    var commit = _ref.commit;

    //  debugger
    if (data.error) {
      window.alert(data.error);
      return;
    }
    commit('applyStartGame', data.ok);
  },
  game: function game(data, _ref2) {
    var commit = _ref2.commit;

    if (data.error) {
      window.alert(data.error);
      return;
    }
    commit('applyGame', data.ok);
  },
  restartRound: function restartRound(data, _ref3) {
    var commit = _ref3.commit;

    //  debugger
    if (data.error) {
      window.alert(data.error);
      return;
    }
    commit('applyRoundRestart', data.ok);
  },
  moveToNextRound: function moveToNextRound(data, _ref4) {
    var commit = _ref4.commit;

    //  debugger
    if (data.error) {
      window.alert(data.error);
      return;
    }
    commit('applyStartedNewRound', data.ok);
  },
  playCard: function playCard(data, _ref5) {
    var commit = _ref5.commit;

    //  debugger
    if (data.error) {
      commit('applyMessage', { message: data.error, type: 'Error' });
      return;
    }
    commit('applyPlayed', data.ok.Played);
  },
  pass: function pass(data, _ref6) {
    var commit = _ref6.commit;

    //  debugger
    if (data.error) {
      commit('applyMessage', { message: data.error, type: 'Error' });
      return;
    }
    commit('applyPassPosted', data.ok.PassPosted);
  },
  thani: function thani(data, _ref7) {
    var commit = _ref7.commit;

    //  debugger
    if (data.error) {
      commit('applyMessage', { message: data.error, type: 'Error' });
      return;
    }
    commit('applyThaniPosted', data.ok.ThaniPosted);
  },
  openThurupu: function openThurupu(data, _ref8) {
    var commit = _ref8.commit;

    //  debugger
    if (data.error) {
      commit('applyMessage', { message: data.error, type: 'Error' });
      return;
    }
    commit('applyOpenThurupuPosted', data.ok.OpenThurupuPosted);
  },
  bid: function bid(data, _ref9) {
    var commit = _ref9.commit;

    //  debugger
    if (data.error) {
      commit('applyMessage', { message: data.error, type: 'Error' });
      return;
    }
    commit('applyBidPosted', {
      bid: data.ok.BidPosted,
      trump: { rank: getParameterByName('rank', data.action.parameters), suit: getParameterByName('suit', data.action.parameters) }
    });
  },
  getNext: function getNext(data, _ref10) {
    var commit = _ref10.commit;

    //  debugger
    if (data.error) {
      window.alert(data.error);
    } else if (data.ok.Played) {
      commit('applyPlayed', data.ok.Played);
    } else if (data.ok.OpenThurupuPosted) {
      commit('applyOpenThurupuPosted', data.ok.OpenThurupuPosted);
    } else if (data.ok.PassPosted) {
      commit('applyPassPosted', data.ok.PassPosted);
    } else if (data.ok.BidPosted) {
      commit('applyBidPosted', { bid: data.ok.BidPosted });
    } else if (data.ok === 'EndedGame') {
      window.alert('game ended');
    } else if (data.StartedNewRound) {
      commit('applyStartedNewRound', data.StartedNewRound);
    } else if (data.MovedToNextRound) {
      commit('applyMovedToNextRound', data.MovedToNextRound);
    } else if (data.RestartedRound) {
      commit('applyRestartedRound', data.RestartedRound);
    } else {
      throw new Error('unable to process result from getNext');
    }
  }
};
var socket = null;
var socketReady = false;
var socketWait = 0;
var parseResult = function parseResult(data) {
  /* if (!result.error) {
    result.ok = JSON.parse(result.ok)
  }
  return result
  */
  // changing Ok and Error to camel Case
  var parsedResult = JSON.parse(data.result);
  return { ok: parsedResult.Ok, error: parsedResult.Error };
};
var initWs = function initWs(store) {
  // Create WebSocket connection.
  socket = new WebSocket('ws://' + window.location.host + '/api/ws?userId=' + store.state.game.playerId);

  // Listen for messages
  socket.addEventListener('message', function (event) {
    //  console.log('Message from server : ' + event.data)
    var eventData = JSON.parse(event.data);
    if (eventData.messageType === 3 && eventData.data.action) {
      commits[eventData.data.action.actionType](parseResult(eventData.data), store);
    } else {
      console.log('message : ' + event.data);
    }
  });

  // Connection opened
  socket.addEventListener('open', function (event) {
    //  socket.send('Hello Server!')
    socketReady = true;
  });

  // Connection closed
  socket.addEventListener('close', function (event) {
    socketReady = false;
  });
};
var getParameterByName = function getParameterByName(name, url) {
  // debugger
  return url.substr(url.indexOf(name + '=')).split('&')[0].split('=')[1];
};
var param = function param(object) {
  var encodedString = '';
  for (var prop in object) {
    if (object.hasOwnProperty(prop)) {
      if (encodedString.length > 0) {
        encodedString += '&';
      }
      encodedString += encodeURI(prop + '=' + object[prop]);
    }
  }
  return encodedString;
};
var sendWsAction = function sendWsAction(type, data) {
  if (socketWait > 5) {
    console.log('socket connection error');
    return;
  }
  if (!socketReady) {
    socketWait++;
    window.setTimeout(function () {
      sendWsAction(type, data);
    }, 500);
    return;
  }
  socketWait = 0;
  socket.send(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify___default()({ ActionType: type, Parameters: data }));
};
var post = function post(url, data) {
  return new window.Promise(function (resolve, reject) {
    var request = new window.XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send(param(data));
    request.onload = function () {
      if (request.status === 200) {
        resolve(window.JSON.parse(request.responseText));
      }
      reject(request);
    };
  });
};

var get = function get(url) {
  return new window.Promise(function (resolve, reject) {
    var request = new window.XMLHttpRequest();
    request.open('GET', url, true);
    request.send();
    request.onload = function () {
      if (request.status === 200) {
        resolve(window.JSON.parse(request.responseText));
      }
      reject(request);
    };
  });
};



/***/ }),

/***/ "hlbm":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"app"}},[_c('game')],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ "jSyP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__suit__ = __webpack_require__("Wbmn");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__minicard__ = __webpack_require__("4s90");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("M4fF");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vuex__ = __webpack_require__("NYxO");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_game__ = __webpack_require__("wOfB");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'player',
  components: { Minicard: __WEBPACK_IMPORTED_MODULE_1__minicard__["a" /* default */], Suit: __WEBPACK_IMPORTED_MODULE_0__suit__["a" /* default */] },
  props: ['player', 'playerNumber'],
  computed: Object(__WEBPACK_IMPORTED_MODULE_3_vuex__["b" /* mapState */])({
    'isThaniBid': function isThaniBid(state) {
      return __WEBPACK_IMPORTED_MODULE_4__store_game__["b" /* Round */].isThaniBid(state.game.round);
    },
    'playStarted': function playStarted(state) {
      return __WEBPACK_IMPORTED_MODULE_4__store_game__["b" /* Round */].playCount(state.game.round) > 0;
    },
    'bidSuit': function bidSuit(state) {
      return state.game.round.bid.thurupu.suit;
    },
    isDealer: function isDealer(state) {
      return this.player.playerId === state.game.round.startingPlayerId;
    },
    showBidInfo: function showBidInfo(state) {
      return this.player.playerId === state.game.round.bid.bidderId && !this.isThaniBid;
    },
    bidText: function bidText(state) {
      if (this.showBidInfo) {
        return state.game.round.bid.value;
      }
      return '';
    },
    isCurrentPlayer: function isCurrentPlayer(state) {
      return this.player.playerId === state.game.round.currentPlayerId;
    },
    tricks: function tricks(state) {
      var playCount = __WEBPACK_IMPORTED_MODULE_4__store_game__["b" /* Round */].playCount(state.game.round);
      if (!playCount) {
        return [];
      }
      var setIndex = Math.floor((playCount - 1) / 6);
      return Object(__WEBPACK_IMPORTED_MODULE_2_lodash__["filter"])(state.game.round.sets[setIndex].tricks, { playerId: this.player.playerId });
    }
  })
});

/***/ }),

/***/ "mUbh":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services__ = __webpack_require__("fU1Q");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store__ = __webpack_require__("wtEF");



var initStoreAndStartGame = function initStoreAndStartGame(commit, state, gameId) {
  commit('applyGameId', gameId);
  __WEBPACK_IMPORTED_MODULE_0__services__["b" /* initWs */](__WEBPACK_IMPORTED_MODULE_1__store__["a" /* default */]);
  __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('startGame', { gameId: state.game.gameId, userId: state.game.playerId });
};

var joinGame = function joinGame(state) {
  return __WEBPACK_IMPORTED_MODULE_0__services__["c" /* post */]('api/room/join', { userId: state.game.playerId, gameId: state.game.gameId, userName: 'PLAYER' });
};

var actions = {
  init: function init(_ref) {
    var commit = _ref.commit,
        state = _ref.state;

    __WEBPACK_IMPORTED_MODULE_0__services__["a" /* get */]('api/room?userId=' + state.game.playerId).then(function (data) {
      var gameId = data.id;
      if (data.players) {
        initStoreAndStartGame(commit, state, gameId);
      } else {
        joinGame(state).then(function (data) {
          initStoreAndStartGame(commit, state, gameId);
        });
      }
    }, function (request) {
      if (request.status === 404) {
        __WEBPACK_IMPORTED_MODULE_0__services__["c" /* post */]('api/room/add', { userId: state.game.playerId, gameId: state.game.gameId }).then(function (data) {
          var gameId = data;
          joinGame(state).then(function (data) {
            initStoreAndStartGame(commit, state, gameId);
          });
        });
      }
    });
  },
  startGame: function startGame(_ref2) {
    var commit = _ref2.commit,
        state = _ref2.state;

    /*
    services.post('api/startGame', {gameId: state.game.gameId, userId: state.game.playerId})
      .then(function (data) {
      //  debugger
        if (data.Error) {
          window.alert(data.Error)
          return
        }
        commit('applyStartGame', data.Ok)
      })
      */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('startGame', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  getGame: function getGame(_ref3) {
    var commit = _ref3.commit,
        state = _ref3.state;

    /*
    services.post('api/game', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyGame', data.Ok)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('game', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  restartRound: function restartRound(_ref4) {
    var commit = _ref4.commit,
        state = _ref4.state;

    /*
    services.post('api/restartRound', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyRoundRestart', data.Ok)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('restartRound', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  moveToNextRound: function moveToNextRound(_ref5) {
    var commit = _ref5.commit,
        state = _ref5.state;

    /*
    services.post('api/moveToNextRound', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
        return
      }
      commit('applyStartedNewRound', data.Ok)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('moveToNextRound', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  playCard: function playCard(_ref6, card) {
    var commit = _ref6.commit,
        state = _ref6.state;

    /*
    services.post('api/playCard', {rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyPlayed', data.Ok.Played)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('playCard', { rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId });
  },
  pass: function pass(_ref7) {
    var commit = _ref7.commit,
        state = _ref7.state;

    /*
    services.post('api/pass', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyPassPosted', data.Ok.PassPosted)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('pass', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  thani: function thani(_ref8) {
    var commit = _ref8.commit,
        state = _ref8.state;

    /*
    services.post('api/thani', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyThaniPosted', data.Ok.ThaniPosted)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('thani', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  openThurupu: function openThurupu(_ref9) {
    var commit = _ref9.commit,
        state = _ref9.state;

    /*
    services.post('api/openThurupu', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyOpenThurupuPosted', data.Ok.OpenThurupuPosted)
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('openThurupu', { gameId: state.game.gameId, userId: state.game.playerId });
  },
  bid: function bid(_ref10, _ref11) {
    var commit = _ref10.commit,
        state = _ref10.state;
    var _bid = _ref11.bid,
        card = _ref11.card;

    /*
    services.post('api/bid', {bid: bid, rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        commit('applyMessage', {message: data.Error, type: 'Error'})
        return
      }
      commit('applyBidPosted', {bid: data.Ok.BidPosted, trump: card})
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('bid', { bid: _bid, rank: card.rank, suit: card.suit, gameId: state.game.gameId, userId: state.game.playerId });
  },
  addGame: function addGame(_ref12) {
    var commit = _ref12.commit,
        state = _ref12.state;

    __WEBPACK_IMPORTED_MODULE_0__services__["c" /* post */]('api/room/add', { userId: state.game.playerId }).then(function (data) {
      //  debugger
      if (data.Error) {}
    });
  },
  getNext: function getNext(_ref13) {
    var commit = _ref13.commit,
        state = _ref13.state;

    /*
    services.post('api/getNext', {gameId: state.game.gameId, userId: state.game.playerId})
    .then(function (data) {
      //  debugger
      if (data.Error) {
        window.alert(data.Error)
      } else if (data.Ok.Played) {
        commit('applyPlayed', data.Ok.Played)
      } else if (data.Ok.OpenThurupuPosted) {
        commit('applyOpenThurupuPosted', data.Ok.OpenThurupuPosted)
      } else if (data.Ok.PassPosted) {
        commit('applyPassPosted', data.Ok.PassPosted)
      } else if (data.Ok.BidPosted) {
        commit('applyBidPosted', {bid: data.Ok.BidPosted})
      } else if (data.Ok === 'EndedGame') {
        window.alert('game ended')
      } else if (data.StartedNewRound) {
        commit('applyStartedNewRound', data.StartedNewRound)
      } else if (data.MovedToNextRound) {
        commit('applyMovedToNextRound', data.MovedToNextRound)
      } else if (data.RestartedRound) {
        commit('applyRestartedRound', data.RestartedRound)
      } else {
        throw new Error('unable to process result from getNext')
      }
    })
    */
    __WEBPACK_IMPORTED_MODULE_0__services__["d" /* sendWsAction */]('getNext', { gameId: state.game.gameId, userId: state.game.playerId });
  }
};
/* harmony default export */ __webpack_exports__["a"] = (actions);

/***/ }),

/***/ "rPrZ":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "tZZY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Game_vue__ = __webpack_require__("RcUN");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_54a3a31c_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Game_vue__ = __webpack_require__("dwS6");
function injectStyle (ssrContext) {
  __webpack_require__("d2rk")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */

/* template */

/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-54a3a31c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Game_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_54a3a31c_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Game_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ "ukYY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__game__ = __webpack_require__("wOfB");


var applyGame = function applyGame(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["a" /* Game */].hydrate(data, state.game);
};
var applyRound = function applyRound(state, data) {
  var event = data.RestartedRound || data.MovedToNextRound || data.StartedNewRound;
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].hydrate(event ? event[1] : data, state.game.round);
};
var applyPlayed = function applyPlayed(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyPlayed(data[0], data[1], '', state.game.round);
};
var applyOpenThurupuPosted = function applyOpenThurupuPosted(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyOpenThurupuPosted(data[0], data[1], state.game.round);
};
var applyPassPosted = function applyPassPosted(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyPassPosted(data[0], data[1], state.game.round);
};
var applyThaniPosted = function applyThaniPosted(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyThaniPosted(data[0], data[1], state.game.round);
};
var applyBidPosted = function applyBidPosted(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyBidPosted(data.bid[0], data.bid[1], data.bid[2], data.trump, state.game.round);
};
var applyMessage = function applyMessage(state, data) {
  __WEBPACK_IMPORTED_MODULE_0__game__["b" /* Round */].applyMessage(data.message, data.type, state.game.round);
};
var applyGameId = function applyGameId(state, gameId) {
  state.game.gameId = gameId;
};
var mutations = {
  applyGame: applyGame,
  applyStartGame: applyGame,
  applyRoundRestart: applyRound,
  applyMovedToNextRound: applyRound,
  applyStartedNewRound: applyRound,
  applyPlayed: applyPlayed,
  applyOpenThurupuPosted: applyOpenThurupuPosted,
  applyPassPosted: applyPassPosted,
  applyThaniPosted: applyThaniPosted,
  applyBidPosted: applyBidPosted,
  applyMessage: applyMessage,
  applyGameId: applyGameId
};
/* harmony default export */ __webpack_exports__["a"] = (mutations);

/***/ }),

/***/ "wOfB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Game; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Round; });
/* unused harmony export Player */
/* unused harmony export Bid */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck__ = __webpack_require__("Zrlr");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass__ = __webpack_require__("wxAW");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("M4fF");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);




var Hand = function () {
  function Hand() {
    __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default()(this, Hand);

    this.type = null;
    this.suit = null;
    this.rank = null;
  }

  __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default()(Hand, null, [{
    key: 'hydrate',
    value: function hydrate(type, data, hand) {
      hand.type = type;
      hand.suit = data.suit;
      hand.rank = data.rank;
    }
  }, {
    key: 'applyPlayed',
    value: function applyPlayed(card, type, hand) {
      hand.type = type;
      hand.suit = card.suit;
      hand.rank = card.rank;
    }
  }, {
    key: 'applyType',
    value: function applyType(type, hand) {
      hand.type = type;
    }
  }]);

  return Hand;
}();

var Player = function () {
  function Player(isUser) {
    __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default()(this, Player);

    this.ai = false;
    this.isUser = isUser || false;
    this.kunukus = 0;
    this.name = '';
    this.playerId = '';
    this.ready = false;
    this.teamId = 0;
    this.askedForThurupuPlayIndex = 0;
    this.hand = [];
    this.message = '';
  }

  __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default()(Player, null, [{
    key: 'hydrate',
    value: function hydrate(data, player) {
      player.ai = data.ai;
      player.kunukus = data.kunukus;
      player.name = data.name;
      player.playerId = data.playerId;
      player.ready = data.ready;
      player.teamId = data.teamId;
      player.message = '';
      if (player.isUser && data.hand.length > 0) {
        for (var index = 0; index < data.hand.length; index++) {
          var val = data.hand[index];
          if (!player.hand[index]) {
            player.hand.push(new Hand());
          }
          Hand.hydrate('Open', val, player.hand[index]);
        }
      }
    }
  }, {
    key: 'applyMessage',
    value: function applyMessage(msg, player) {
      player.message = msg;
    }
  }, {
    key: 'findHand',
    value: function findHand(card, player) {
      for (var i = 0; i < player.hand.length; i++) {
        var element = player.hand[i];
        if (card.suit === element.suit && card.rank === element.rank) {
          return element;
        }
      }
    }
  }, {
    key: 'applyPlayedHand',
    value: function applyPlayedHand(card, player) {
      if (player.isUser) {
        Hand.applyPlayed(card, 'Hidden', Player.findHand(card, player));
      }
    }
  }, {
    key: 'applyTrumpCard',
    value: function applyTrumpCard(card, player) {
      if (player.isUser) {
        var hand = Player.findHand(card, player);
        if (hand) {
          Hand.applyType('Trump', hand);
        }
      }
    }
  }, {
    key: 'applyOpenedTrumpCard',
    value: function applyOpenedTrumpCard(card, player) {
      if (player.isUser) {
        var hand = Player.findHand(card, player);
        if (hand) {
          Hand.applyType('Open', hand);
        }
      }
    }
  }, {
    key: 'askedForThurupuPlayIndex',
    value: function askedForThurupuPlayIndex(_askedForThurupuPlayIndex, player) {
      player.askedForThurupuPlayIndex = _askedForThurupuPlayIndex;
    }
  }]);

  return Player;
}();

var Bid = function () {
  function Bid() {
    __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default()(this, Bid);

    this.status = null;
    this.bidderId = null;
    this.value = null;
    this.openedPlayerId = null;
    this.openedSetId = null;
    this.openedTrickId = null;
    this.openedAt = null;
    this.passCount = 0;
    this.thurupu = {
      suit: null,
      rank: null
    };
  }

  __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default()(Bid, null, [{
    key: 'reset',
    value: function reset(bid) {
      bid.status = null;
      bid.bidderId = null;
      bid.value = null;
      bid.openedPlayerId = null;
      bid.openedSetId = null;
      bid.openedTrickId = null;
      bid.openedAt = null;
      bid.thurupu.suit = null;
      bid.thurupu.rank = null;
      bid.passCount = 0;
    }
  }, {
    key: 'hydrate',
    value: function hydrate(data, bid) {
      Bid.reset(bid);
      if (data === 'WaitingForBid') {
        bid.status = 'WaitingForBid';
      } else if (data.ProcessingBid) {
        bid.status = 'ProcessingBid';
        bid.bidderId = data.ProcessingBid.bidderId;
        bid.value = data.ProcessingBid.value;
      } else if (data.ThaniBid) {
        bid.status = 'ThaniBid';
        bid.bidderId = data.ThaniBid;
      } else if (data.Bid) {
        bid.status = 'Bid';
        bid.bidderId = data.Bid.bidderId;
        bid.value = data.Bid.value;
      } else if (data.OpenedBid) {
        bid.status = 'OpenedBid';
        bid.bidderId = data.OpenedBid[0].bidderId;
        bid.value = data.OpenedBid[0].value;
        bid.openedPlayerId = data.OpenedBid[1].openedPlayerId;
        bid.openedSetId = data.OpenedBid[1].openedSetId;
        bid.openedTrickId = data.OpenedBid[1].openedTrickId;
        bid.openedAt = data.OpenedBid[1].openedAt;
        bid.thurupu.suit = data.OpenedBid[0].thurupu.suit;
        bid.thurupu.rank = data.OpenedBid[0].thurupu.rank;
      } else {
        throw new Error('unable to hydrate bid');
      }
    }
  }, {
    key: 'apply',
    value: function apply(data, type, bid) {
      switch (type) {
        case 'WaitingForBid':
          bid.status = 'WaitingForBid';
          break;
        case 'ProcessingBid':
          bid.status = 'ProcessingBid';
          bid.bidderId = data.bidderId;
          bid.value = data.value;
          bid.passCount = 0;
          break;
        case 'Pass':
          bid.passCount++;
          //  if 5 passed then change from ProcessingBid to Bid
          if (bid.passCount === 5) {
            bid.status = 'Bid';
          }
          break;
        case 'ThaniBid':
          bid.status = 'ThaniBid';
          bid.bidderId = data.bidderId;
          break;
        case 'Bid':
          bid.status = 'Bid';
          bid.bidderId = data.bidderId;
          bid.value = data.value;
          break;
        case 'OpenedBid':
          bid.status = 'OpenedBid';
          bid.openedPlayerId = data.openedPlayerId;
          bid.openedSetId = data.openedSetId;
          bid.openedTrickId = data.openedTrickId;
          bid.openedAt = data.openedAt;
          bid.thurupu.suit = data.thurupu.suit;
          bid.thurupu.rank = data.thurupu.rank;
          break;
        default:
          throw new Error('unknown bid status');
      }
    }
  }]);

  return Bid;
}();

var Round = function () {
  function Round(playerId) {
    __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default()(this, Round);

    this.playerId = playerId;
    this.status = null;
    this.bid = new Bid();
    this.currentPlayerId = null;
    this.playerIndexMap = {};
    this.currentTrickIndex = 0;
    this.currentSetIndex = 0;
    this.id = '';
    this.players = [new Player(true), new Player(), new Player(), new Player(), new Player(), new Player()];
    this.sets = [];
    this.startingPlayerId = null;
    this.startingPlayerIndex = 0;
    this.message = {
      text: '',
      type: ''
    };
  }

  __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default()(Round, null, [{
    key: 'hydrate',
    value: function hydrate(data, round) {
      round.startingPlayerIndex = data.startingPlayerIndex;
      round.startingPlayerId = data.startingPlayerId;
      //  reverse sets and tricks
      for (var i = 0; i < data.sets.length; i++) {
        var set = data.sets[i];
        set.tricks.reverse();
      }
      round.message = {
        text: '',
        type: ''
      };
      round.sets = data.sets.reverse();
      round.id = data.id;
      Bid.hydrate(data.bid, round.bid);
      // round.bid.hydrate(data.bid)
      round.currentTrickIndex = data.currentTrickIndex;
      round.currentSetIndex = data.currentSetIndex;
      round.currentPlayerId = data.currentPlayerId;
      round.status = data.status;
      round.playerIndexMap = {};
      var userPlayerIndex = Object(__WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"])(data.players, { playerId: round.playerId });
      for (var index = 0; index < data.players.length; index++) {
        var playerIndex = (userPlayerIndex + index) % 6;
        Player.hydrate(data.players[index], round.players[playerIndex]);
        //  set current player index
        round.playerIndexMap[data.players[playerIndex].playerId] = index;
      }
    }
  }, {
    key: 'isThaniBid',
    value: function isThaniBid(round) {
      return round.bid.status === 'ThaniBid';
    }
  }, {
    key: 'biddingCompleted',
    value: function biddingCompleted(round) {
      return round.bid.status === 'Bid' || round.bid.status === 'ThaniBid' || round.bid.status === 'OpenedBid';
    }
  }, {
    key: 'playCount',
    value: function playCount(round) {
      return 6 * round.currentSetIndex + round.currentTrickIndex;
    }
  }, {
    key: 'currentPlayerIndex',
    value: function currentPlayerIndex(round) {
      return round.playerIndexMap[round.currentPlayerId];
    }
  }, {
    key: 'isUserCurrentPlayer',
    value: function isUserCurrentPlayer(round) {
      return round.playerId === round.currentPlayerId;
    }
  }, {
    key: 'setPlayerMessage',
    value: function setPlayerMessage(msg, playerId, round) {
      Player.applyMessage(msg, round.players[round.playerIndexMap[playerId]]);
    }
  }, {
    key: 'moveToNextPlayer',
    value: function moveToNextPlayer(userId, reason, currentPlayerId, round) {
      //  window.console.log('current player ' + reason + ', moving to next player:' + currentPlayerId)
      round.currentPlayerId = currentPlayerId;
    }
  }, {
    key: 'applyMessage',
    value: function applyMessage(message, type, round) {
      round.message.text = message;
      round.message.type = type;
    }
  }, {
    key: 'applyPlayed',
    value: function applyPlayed(userId, set, type, round) {
      var currentPlayerIndex = Round.currentPlayerIndex(round);
      Player.applyPlayedHand(set.trick.playedCard, round.players[currentPlayerIndex]);
      Round.moveToNextPlayer(userId, type + ' card', set.currentPlayerId, round);
      round.currentTrickIndex = set.currentTrickIndex;
      if (round.sets.length - 1 < set.currentSetIndex || round.sets.length === 0) {
        round.currentSetIndex = set.currentSetIndex;
        round.sets.push({
          completedAt: set.completedAt,
          currentPlayerId: set.currentPlayerId,
          cutBy: set.cutBy,
          isCut: set.isCut,
          setId: set.setId,
          winner: set.winner,
          tricks: []
        });
      } else {
        var s = round.sets[round.currentSetIndex];
        s.completedAt = set.completedAt;
        s.cutBy = set.cutBy;
        s.isCut = set.isCut;
        s.setId = set.setId;
        s.winner = set.winner;
      }
      if (round.currentSetIndex > 0 && round.currentTrickIndex === 0) {
        //  set just ended
        round.sets[round.currentSetIndex - 1].tricks.push(set.trick);
        Round.applyMessage('🎉 ' + round.players[round.playerIndexMap[set.winner]].name + ' is the winner 🎉', '', round);
      } else {
        round.sets[round.currentSetIndex].tricks.push(set.trick);
        Round.applyMessage('', '', round);
      }
      if (Round.playCount(round) === 36) {
        round.status = 'Ended';
      }
    }
  }, {
    key: 'applyOpenThurupuPosted',
    value: function applyOpenThurupuPosted(userId, thurupu, round) {
      Round.applyMessage('Opened Thurupu', '', round);
      Bid.apply({ openedPlayerId: userId, thurupu: thurupu }, 'OpenedBid', round.bid);
      if (round.bid.bidderId === round.playerId && thurupu) {
        Player.applyOpenedTrumpCard(thurupu, round.players[round.playerIndexMap[round.playerId]]);
      }
    }
  }, {
    key: 'applyPassPosted',
    value: function applyPassPosted(userId, currentPlayerId, round) {
      Round.setPlayerMessage('Pass', userId, round);
      Round.applyMessage('', '', round);
      Round.moveToNextPlayer(userId, 'posted pass', currentPlayerId, round);
      Bid.apply({}, 'Pass', round.bid);
    }
  }, {
    key: 'applyThaniPosted',
    value: function applyThaniPosted(userId, currentPlayerId, round) {
      Round.setPlayerMessage('Thani', userId, round);
      Round.applyMessage('', '', round);
      Round.moveToNextPlayer(userId, 'posted thani', currentPlayerId, round);
      Bid.apply({ bidderId: userId }, 'ThaniBid', round.bid);
    }
  }, {
    key: 'applyBidPosted',
    value: function applyBidPosted(userId, bid, currentPlayerId, thurupu, round) {
      Round.setPlayerMessage('Bid', userId, round);
      Round.applyMessage('', '', round);
      Round.moveToNextPlayer(userId, 'posted bid', currentPlayerId, round);
      Bid.apply(bid, 'ProcessingBid', round.bid);
      if (round.bid.bidderId === round.playerId && thurupu) {
        Player.applyTrumpCard(thurupu, round.players[round.playerIndexMap[round.playerId]]);
      }
    }
  }]);

  return Round;
}();

var Game = function () {
  function Game(playerId, gameId) {
    __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_classCallCheck___default()(this, Game);

    this.playerId = playerId;
    this.gameId = gameId;
    this.team1 = null;
    this.team2 = null;
    this.round = new Round(this.playerId);
  }

  __WEBPACK_IMPORTED_MODULE_1_babel_runtime_helpers_createClass___default()(Game, null, [{
    key: 'hydrate',
    value: function hydrate(data, game) {
      Round.hydrate(data.round, game.round);
    }
  }]);

  return Game;
}();



/***/ }),

/***/ "wtEF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("7+uW");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuex__ = __webpack_require__("NYxO");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__game__ = __webpack_require__("wOfB");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mutations__ = __webpack_require__("ukYY");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions__ = __webpack_require__("mUbh");






__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_1_vuex__["a" /* default */]);
var gameId = 'c46f8f2d-54d1-4c05-932c-bc9b864fac60';
var playerId = 'c46f8f2d-54d1-4c05-932c-bc9b864fac60';
var store = new __WEBPACK_IMPORTED_MODULE_1_vuex__["a" /* default */].Store({
  state: {
    game: new __WEBPACK_IMPORTED_MODULE_2__game__["a" /* Game */](gameId, playerId)
  },
  getters: {},
  mutations: __WEBPACK_IMPORTED_MODULE_3__mutations__["a" /* default */],
  actions: __WEBPACK_IMPORTED_MODULE_4__actions__["a" /* default */]
});
/* harmony default export */ __webpack_exports__["a"] = (store);

/***/ }),

/***/ "xCgB":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "xJD8":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Game__ = __webpack_require__("tZZY");
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'app',
  components: {
    Game: __WEBPACK_IMPORTED_MODULE_0__components_Game__["a" /* default */]
  }
});

/***/ }),

/***/ "zmmk":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

},["NHnr"]);
//# sourceMappingURL=app.750f824980a60dd334d5.js.map