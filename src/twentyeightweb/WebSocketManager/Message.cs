namespace twentyeightweb.WebSocketManager
{
    public enum MessageType
    {
        Text = 1,
        ConnectionEvent,
        GameEvent
    }

    public class Message<T>
    {
        public MessageType MessageType { get; set; }
        public T Data { get; set; }
    }
}