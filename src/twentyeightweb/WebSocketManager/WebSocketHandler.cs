using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace twentyeightweb.WebSocketManager
{
    public abstract class WebSocketHandler
    {
        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        private static JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };
        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager)
        {
            WebSocketConnectionManager = webSocketConnectionManager;
        }

        public virtual async Task OnConnected(WebSocket socket)
        {
            WebSocketConnectionManager.AddSocket(socket);
            await SendMessageToAllAsync(new Message<string>()
            {
                MessageType = MessageType.ConnectionEvent,
                Data = $"{WebSocketConnectionManager.GetId(socket)} - connected"
            });
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            var message = new Message<string>()
            {
                MessageType = MessageType.ConnectionEvent,
                Data = $"{WebSocketConnectionManager.GetId(socket)} - disconnected"
            };
            await WebSocketConnectionManager.RemoveSocket(WebSocketConnectionManager.GetId(socket));
            await SendMessageToAllAsync(message);
            socket.Dispose(); 
        }

        public async Task SendMessageAsync<T>(WebSocket socket, Message<T> message)
        {
            if (socket.State != WebSocketState.Open)
            {
                return;
            }   

            var serializedMessage = JsonConvert.SerializeObject(message, _jsonSerializerSettings);
            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.ASCII.GetBytes(serializedMessage),
                                                                  offset: 0,
                                                                  count: serializedMessage.Length),
                                   messageType: WebSocketMessageType.Text,
                                   endOfMessage: true,
                                   cancellationToken: CancellationToken.None);
        }

        public async Task SendMessageAsync<T>(string socketId, Message<T> message)
        {
            await SendMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message);
        }

        public async Task SendMessageToAllAsync<T>(Message<T> message)
        {
            foreach (var pair in WebSocketConnectionManager.GetAll())
            {
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message);
            }
        }

        public virtual async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            await SendMessageToAllAsync(new Message<string>
            {   Data=Encoding.UTF8.GetString(buffer, 0, result.Count), 
                MessageType=MessageType.Text
            });
        }
    }
}