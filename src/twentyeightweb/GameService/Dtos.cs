using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace twentyeightweb.GameServices
{
    public class ActionDto 
    {
        public string ActionType { get; set; }
        [JsonConverter(typeof(BlobJsonConverter))]
        public string Parameters { get; set; }
    }
    public class ResultDto 
    {
        [JsonConverter(typeof(BlobJsonConverter))]
        public string Error { get; set; }
        
        [JsonConverter(typeof(BlobJsonConverter))]
        public string Ok { get; set; }

        public ActionDto Action { get; set; }
    }

    public class RpcResult 
    {
        [JsonConverter(typeof(BlobJsonConverter))]
        public string Result { get; set; }

        public ActionDto Action { get; set; }
    }

    public class BlobJsonConverter: JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(string));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            return token.ToString(Formatting.None);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken token = JToken.FromObject(value);
            token.WriteTo(writer);
        }
    }
}