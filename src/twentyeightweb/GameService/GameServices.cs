using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace twentyeightweb.GameServices
{
    public class GameProxyService
    {
        private HttpClient _client;

        public GameProxyService()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("http://service:8082");
        }
        public async Task<ResultDto> Post(ActionDto action)
        {
            var response = await _client.PostAsync($"api/{action.ActionType}", new StringContent(action.Parameters));
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<ResultDto>(await response.Content.ReadAsStringAsync());
            result.Action = action;
            return result;
        }

        public async Task<ResultDto> Post(string actionType, Dictionary<string, string> parameters)
        {
            var response = await _client.PostAsync($"api/{actionType}", new FormUrlEncodedContent(parameters));
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<ResultDto>(await response.Content.ReadAsStringAsync());
        }
    }
}