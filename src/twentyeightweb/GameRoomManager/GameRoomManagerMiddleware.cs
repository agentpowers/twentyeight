using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using twentyeightweb.WebSocketManager;

namespace twentyeightweb.GameRoomManager
{
    public class GameRoomManagerMiddleware 
    {

        private readonly RequestDelegate _next;
        private IGameRoomManager _gameRoomManager { get; set; }

        public GameRoomManagerMiddleware(RequestDelegate next, 
                                          IGameRoomManager gameRoomManager)
        {
            _next = next;
            _gameRoomManager = gameRoomManager;
        }
        static Guid tempId = Guid.NewGuid();
        private WebSocketHandler GetWebSocketHandler(Guid id)
        {
            return _gameRoomManager.GetRoom(id);
        }

        public async Task Invoke(HttpContext context)
        {
            if(!context.WebSockets.IsWebSocketRequest)
            {
                return;
            }
            
            var socket = await context.WebSockets.AcceptWebSocketAsync();
            await GetWebSocketHandler(GetUserId(context)).OnConnected(socket);
            
            await Receive(socket, async(result, buffer) =>
            {
                if(result.MessageType == WebSocketMessageType.Text)
                {
                    await GetWebSocketHandler(GetUserId(context)).ReceiveAsync(socket, result, buffer);
                    return;
                }

                else if(result.MessageType == WebSocketMessageType.Close)
                {
                    await GetWebSocketHandler(GetUserId(context)).OnDisconnected(socket);
                    return;
                }

            });
            
            // Call the next delegate/middleware in the pipeline
            await this._next(context);
        }

        private static async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while(socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                                                       cancellationToken: CancellationToken.None);

                handleMessage(result, buffer);                
            }
        }

        private static Guid GetUserId(HttpContext context)
        {
            Guid userId;
            if(!Guid.TryParse(context.Request.Query["userId"],out userId))
            {
                throw new InvalidOperationException("invalid user id");
            }
            return userId;
        }
    }
}