
using System;
using System.Collections.Concurrent;
using System.Linq;
using TwentyEight.Web;

namespace twentyeightweb.GameRoomManager
{
    public class GameRoomManager : IGameRoomManager
    {
        public GameRoomManager(Interop.TwentyEightEngine twentyEightEngine)
        {
            _twentyEightEngine = twentyEightEngine;
        }
        private static readonly ConcurrentDictionary<Guid, GameRoom> _gameRooms = new ConcurrentDictionary<Guid, GameRoom>();
        private static readonly ConcurrentDictionary<Guid, Guid> _userRoomMap = new ConcurrentDictionary<Guid, Guid>();
        
        private Interop.TwentyEightEngine _twentyEightEngine;
        public Interop.TwentyEightEngine TwentyEightEngine{ get { return _twentyEightEngine;}}
        
        public bool AddRoom(Guid userId, Guid gameId)
        {
            if(_userRoomMap.ContainsKey(userId))
            {
                throw new InvalidOperationException("User is already in a room");
            }
            else
            {
                _userRoomMap.TryAdd(userId,gameId);
                var room = new GameRoom(gameId, TwentyEightEngine);
                room.AddPlayer(userId);
                return _gameRooms.TryAdd(gameId,room);
            }
        }

        public Guid[] GetAvailableRooms()
        {
            return _gameRooms.Values.Where(g=>g.Players.Count<6).Take(10).Select(g=>g.Id).ToArray();
        }

        public GameRoom GetRoom(Guid userId)
        {
            if(_userRoomMap.ContainsKey(userId))
            {
                return _gameRooms[_userRoomMap[userId]];
            }
            return null;
        }

        public bool JoinRoom(Guid userId, Guid roomId)
        {
            //check if room exists
            if(!_gameRooms.TryGetValue(roomId, out var room))
            {
                throw new InvalidOperationException("Unable to find room");
            }
            //check if user is in another room
            if(_userRoomMap.TryGetValue(userId, out var userRoomId))
            {
                if(userRoomId != roomId) 
                {
                    throw new InvalidOperationException("User is already in another room");
                }
            }
            else
            {
                _userRoomMap.TryAdd(userId,roomId);
            }
            room.AddPlayer(userId);
            return true;
        }

        public bool LeaveRoom(Guid userId, Guid roomId)
        {
            if(!_userRoomMap.ContainsKey(userId))
            {
                throw new InvalidOperationException("User not part of any game");
            }
            if(_userRoomMap[userId]!=roomId)
            {
                throw new InvalidOperationException("user not in room");
            }
            if(!_gameRooms.TryGetValue(roomId, out var room))
            {
                throw new InvalidOperationException("Unable to find room");
            }
            room.RemovePlayer(userId);
            if(!room.Players.Any())
            {
                _gameRooms.TryRemove(roomId, out room);
            }
            return _userRoomMap.TryRemove(userId, out roomId);

        }

        public bool RemoveRoom(Guid userId)
        {
            if(!_userRoomMap.ContainsKey(userId))
            {
                throw new InvalidOperationException("User not part of any game");
            }
            var room = _gameRooms[_userRoomMap[userId]];
            if(room.Players.Count>0)
            {
                throw new InvalidOperationException("Cannot remove room when there are players in it");
            }
            else
            {
                _userRoomMap.TryRemove(userId, out var gameId);
                return _gameRooms.TryRemove(gameId,out room);
            }
        }
    }    
}
