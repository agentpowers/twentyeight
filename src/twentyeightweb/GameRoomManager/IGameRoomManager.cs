using System;
using TwentyEight.Web;

namespace twentyeightweb.GameRoomManager
{
    public interface IGameRoomManager
    {
        bool AddRoom(Guid userId, Guid gameId);
        bool JoinRoom(Guid userId, Guid roomId);
        bool LeaveRoom(Guid userId, Guid roomId);
        GameRoom GetRoom(Guid userId);
        bool RemoveRoom(Guid userId);
        Guid[] GetAvailableRooms();
        Interop.TwentyEightEngine TwentyEightEngine { get; }
    }
}