using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using twentyeightweb.WebSocketManager;
using twentyeightweb.GameServices;
using TwentyEight.Web;
using Microsoft.Extensions.Dependency­Injection;
namespace twentyeightweb.GameRoomManager
{
    public class GameRoom : WebSocketHandler
    {
        private Guid _id;
        private ConcurrentDictionary<Guid,string> _players;
        private Interop.TwentyEightEngine _twentyEightEngine;
        public GameRoom(Guid id, Interop.TwentyEightEngine twentyEightEngine) : base(new WebSocketConnectionManager())
        {
            _id = id;
            _twentyEightEngine = twentyEightEngine;
            _players = new ConcurrentDictionary<Guid,string>();
        }

        public bool AddPlayer(Guid userId)
        {
            if(_players.Count<6)
            {
                _players.GetOrAdd(userId,userId.ToString());
                return true;
            }
            throw new InvalidOperationException("room is full");
        }

        public ICollection<Guid> Players
        {
            get { return _players.Keys;}
        }

        public bool RemovePlayer(Guid userId)
        {
            return _players.TryRemove(userId, out var val);
        }

        public Guid Id
        {
            get { return _id;}
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            try
            {
                var action = JsonConvert.DeserializeObject<ActionDto>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                try
                {
                    var resp = await _twentyEightEngine.Rpc(action.ActionType, action.Parameters);
                    //var response = await _gameServices.Post(message);
                    await SendMessageAsync(socket,ToGameEventMessage(new RpcResult{ Result = resp, Action = action}));
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    await SendMessageAsync(socket,ToGameEventMessage(ex.Message, action));
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                await SendMessageAsync(socket,ToGameEventMessage(ex.Message));
            }

        }     

        public static Message<RpcResult> ToGameEventMessage(string errorMessage, ActionDto action = null)
        {
            return new Message<RpcResult>{MessageType=MessageType.GameEvent, Data=new RpcResult{ Result= $"{{ error: {errorMessage} }}" , Action=action}};
        }

        public static Message<RpcResult> ToGameEventMessage(RpcResult result)
        {
            return new Message<RpcResult>{MessageType=MessageType.GameEvent, Data=result};
        }
    }
}