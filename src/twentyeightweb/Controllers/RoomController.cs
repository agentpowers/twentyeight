using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using twentyeightweb.GameRoomManager;
using twentyeightweb.GameServices;

namespace twentyeightweb.Controllers
{
    [Route("api/room")]
    public class RoomController : Controller
    {
        IGameRoomManager _gameRoomManager;
        public RoomController(IGameRoomManager gameRoomManager)
        {
            _gameRoomManager = gameRoomManager;
        }

        [Route(""), HttpGet]
        public IActionResult Get(Guid userId)
        {
            var room = _gameRoomManager.GetRoom(userId);
            if(room!=null)
            {
                return Ok(room);
            }
            return NotFound();
        }

        [Route("available")]
        public Guid[] Available()
        {
            return _gameRoomManager.GetAvailableRooms();
        }

        [Route("add")]
        public async Task<IActionResult> Add(Guid userId, Guid? gameId = null)
        {
            try
            {
                var _gameId = gameId.HasValue ? gameId.Value : Guid.NewGuid();
                var result = await _gameRoomManager.TwentyEightEngine.AddGame(_gameId);//await _gameProxyService.Post("addGame", parameters);
                _gameRoomManager.AddRoom(userId, _gameId);
                return Ok(_gameId);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("remove")]
        public bool Remove(Guid userId)
        {
            return _gameRoomManager.RemoveRoom(userId);
        }

        [Route("join")]
        public async Task<IActionResult> Join(Guid userId, Guid gameId, string userName)
        {
            try
            {
                var result = await _gameRoomManager.TwentyEightEngine.JoinMultiPlayerGame(userId,gameId, userName);
                //var result = await _gameProxyService.Post("joinMultiPlayerGame", parameters);
                return Ok(_gameRoomManager.JoinRoom(userId, userId));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("leave")]
        public bool Leave(Guid userId, Guid gameId)
        {
            return _gameRoomManager.LeaveRoom(userId, gameId);
        }
        
    }
}
