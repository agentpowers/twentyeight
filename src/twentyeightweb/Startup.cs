﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TwentyEight.Web;

namespace twentyeightweb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var twentyEightEngine = new Interop.TwentyEightEngine(Configuration.GetConnectionString("PostgresDotNet"));
            var gameRoomManager = new GameRoomManager.GameRoomManager(twentyEightEngine);
            services.AddSingleton(typeof(GameRoomManager.IGameRoomManager), gameRoomManager);
            services.AddMvc();
            //services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseWebSockets();

            app.Map("/api/ws", (_app) => _app.UseMiddleware<twentyeightweb.GameRoomManager.GameRoomManagerMiddleware>());
            
            /*app.UseSignalR(routes =>
            {
                routes.MapHub<twentyeightweb.Hubs.Chat>("chat");
            });*/

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
