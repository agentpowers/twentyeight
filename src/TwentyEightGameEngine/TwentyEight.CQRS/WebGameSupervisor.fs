namespace TwentyEight.CQRS
open System
open TwentyEight.Game
open TwentyEight.Game.SinglePlayer
open TwentyEight.Game.Card
open TwentyEight.Game.Round
open TwentyEight.Game.GameAttributes
open TwentyEight.Core.Global

module WebGameSupervisor=

    type State = {
        Users: WebUser list
        GameId:Guid
        Active: bool
    }

    type Command = 
        | Start of Guid
        | JoinGame of User:WebUser
        | LeaveGame of User:WebUser
        | Active
        | Inactive

    type Event = 
        | Started of Guid
        | JoinedGame of User:WebUser
        | LeftGame of User:WebUser
        | Activated
        | Inactivated
    
    let zero = 
        {Users = list.Empty; GameId=System.Guid.Empty; Active=true}  

    let apply (state:State)= function
        | Started id-> { state with GameId=id} 
        | JoinedGame user -> 
            match state.Users |> List.tryFind (fun u -> u = user) with 
            | Some user -> 
                state
            | None -> {state with Users=user::state.Users}
        | LeftGame user -> 
            match state.Users |> List.tryFind (fun u -> u = user) with 
            | Some user -> 
                {state with Users=state.Users |> List.except [user]}
            | None -> state     
        | Inactivated -> {state with Active=false}  
        | Activated -> {state with Active=true}  


    let exec item = 
        let apply event = 
            let newItem = apply item event
            newItem,event
        function    
        | Start id -> id |> Started |> apply  
        | JoinGame user  -> user |> JoinedGame |> apply 
        | LeaveGame user -> user |> LeftGame |> apply 
        | Active -> Activated |> apply
        | Inactive -> Inactivated |> apply
