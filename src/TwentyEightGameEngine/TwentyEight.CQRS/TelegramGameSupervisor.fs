﻿namespace TwentyEight.CQRS
open System
open TwentyEight.Game
open TwentyEight.Game.SinglePlayer
open TwentyEight.Game.Card
open TwentyEight.Game.Round
open TwentyEight.Game.GameAttributes
open TwentyEight.Core.Global

module TelegramGameSupervisor=

    type State = {
        GameTokens : Map<Guid,int64> //token,chatId
        Route: Map<int64,int64*TelegramUser> //chatId, chatId optional
        User: Map<int64,TelegramUser> //chatId,user
    }

    type Command = 
        | CreateUser of User:TelegramUser
        | CreateGame of User:TelegramUser*Token:Guid
        | RemoveGame of User:TelegramUser*Token:Guid
        | JoinGame of User:TelegramUser*Token:Guid
        | LeaveGame of User:TelegramUser

    type Event = 
        | CreatedUser of User:TelegramUser
        | CreatedGame of User:TelegramUser*Token:Guid
        | RemovedGame of User:TelegramUser*Token:Guid
        | JoinedGame of User:TelegramUser*Token:Guid
        | LeftGame of User:TelegramUser
    
    let zero = 
        {GameTokens =Map.empty; Route=Map.empty; User=Map.empty}  

    let rec removeFromMap ids (map:Map<int64,int64*TelegramUser>) = 
        match ids with
        | head :: tail -> 
            removeFromMap tail (map.Remove(head))
        | [] -> map

    let apply (state:State)= function
        | CreatedUser (user) ->
            { state with User = state.User.Add(user.TelegramId,user)}
        | CreatedGame (user,token) -> 
            let state'=
                    match state.GameTokens |> Map.tryPick (fun key value -> if value=user.TelegramId then (Some key) else None) with
                    |Some token -> 
                        let gameTokens' = state.GameTokens.Remove token    //remove and add
                        { state with GameTokens = gameTokens'.Add(token,user.TelegramId) }
                    |None -> { state with GameTokens = state.GameTokens.Add(token,user.TelegramId) }
            {state' with Route = state'.Route.Add(user.TelegramId,(user.TelegramId,user))}
        | RemovedGame (user,token) -> 
            let routesToRemove = state.Route |> Map.filter (fun a (b,c) -> b=state.GameTokens.Item(token)) |> Map.toList |> List.map (fun (a,(b,c))-> a)
            { state with GameTokens = state.GameTokens.Remove token; Route = state.Route |> removeFromMap routesToRemove}
        | JoinedGame (user,token) -> 
            {state with Route = state.Route.Add(user.TelegramId,(state.GameTokens.Item(token),user))}
        | LeftGame (user) -> 
            {state with Route=state.Route.Remove user.TelegramId}

    let exec item = 
        let apply event = 
            let newItem = apply item event
            newItem,event
        function
        | CreateUser (user)           -> user |> CreatedUser |> apply
        | CreateGame (user, token)    -> (user,token)  |> CreatedGame |> apply                    
        | RemoveGame (user,token)     -> (user,token)  |> RemovedGame |> apply        
        | JoinGame (user,token)       -> (user,token)  |> JoinedGame |> apply 
        | LeaveGame (user)            -> (user) |> LeftGame |> apply 
