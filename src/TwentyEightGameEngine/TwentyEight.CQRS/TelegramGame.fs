﻿namespace TwentyEight.CQRS
open System
open TwentyEight.Game
open TwentyEight.Game.SinglePlayer
open TwentyEight.Game.Card
open TwentyEight.Game.Round
open TwentyEight.Game.GameAttributes
open TwentyEight.Core.Global
open Telegram.Bot.Types.ReplyMarkups

module TelegramGame=

    type UserActionState = 
        | WaitingForStartGame
        | WaitingForBidOrPassOrThani
        | WaitingForBidCard
        | WaitingForBidValue of Card
        | WaitingForNewRound
        | WaitingForPlay
        | WaitingForPlayOrOpenThurupu
        | WaitingForNextRound
        | WaitingForAi of string option

    type MultiPlayerState = {
        Token:Guid
        Players:Map<int64,TelegramUser>
        CurrentPlayer: TelegramUser option
    }

    let initMultiState token user= {
        Token=token;
        Players= Map.ofSeq [(user.TelegramId,user)];
        CurrentPlayer=Some user
    }

    type State = {
        User:TelegramUser
        EditableMessageId:int option
        KeyboardMarkup:InlineKeyboardMarkup option
        SetMessages:Map<int,string>
        UserAction: UserActionState
        //AutoPlayState:Guid
        GameId:Guid
        MultiPlayerState : MultiPlayerState option
        OtherGameToken : Guid option
        CombineAiMessages:bool
    }

    type Command = 
        | Initialize of User:TelegramUser
        | StartSingleGame of GameId:Guid
        | StartMultiGame of GameId:Guid*Token:Guid
        | EndGame 
        | StartNewRound
        | UpdateUserAction of UserActionState:UserActionState
        | UpdateSetMessage of SetId:int*Message:string*KeyboardMarkup:InlineKeyboardMarkup*SetEnded:bool
        //multi player
        | JoinAnotherGame of Token:Guid
        | JoinMultiPlayerGame of User:TelegramUser
        | LeaveMultiPlayerGame of User:TelegramUser
        | UpdateCurrentMultiplayerUser of PlayerId:Guid
        //user UI state commands
        | UpdateEditableMessageId of MessageId:int
        | ClearEditableMessageId

    type Event = 
        | Initialized of User:TelegramUser
        | StartedSingleGame of GameId:Guid
        | StartedMultiGame of GameId:Guid*Token:Guid
        | EndedGame 
        | StartedNewRound
        | UpdatedUserAction of UserActionState:UserActionState
        | UpdatedSetMessage of SetId:int*Message:string*KeyboardMarkup:InlineKeyboardMarkup*SetEnded:bool
        //multi player
        | JoinedAnotherGame of Token:Guid
        | JoinedMultiPlayerGame of User:TelegramUser
        | LeftMultiPlayerGame of User:TelegramUser
        | UpdatedCurrentMultiplayerUser of PlayerId:Guid
        //user UI state commands
        | UpdatedEditableMessageId of MessageId:int
        | ClearedEditableMessageId

    let zero = {
        User={TelegramId=0L;GameUserId=Guid.Empty;TelegramName=""};
        EditableMessageId=None;
        KeyboardMarkup=None
        SetMessages=Map.empty
        UserAction=WaitingForStartGame
        //AutoPlayState=Guid.Empty
        GameId=Guid.Empty
        MultiPlayerState = None;
        OtherGameToken = None;
        CombineAiMessages=false;
    }  
    let getMultiState state =
        state.MultiPlayerState |> Option.get

    let updateMultiPlayerUser currentPlayerId state =
         match state.MultiPlayerState with
            | Some s -> 
               { state with 
                    MultiPlayerState =Some {s with 
                                                CurrentPlayer=s.Players |>Map.tryPick (fun i g -> if g.GameUserId=currentPlayerId then Some g else None) }}
            | None -> state

    let upsertSetMessages gameState id message=
        match gameState.SetMessages |> Map.containsKey id with
        | true -> let map = gameState.SetMessages.Remove(id)
                  map.Add(id,message)
        | false -> gameState.SetMessages.Add(id,message)

    let apply (state:State)= function
        | Initialized user -> { state with User=user}
        | StartedSingleGame id -> {state with 
                                        EditableMessageId=None;
                                        KeyboardMarkup=None
                                        GameId=id;
                                        SetMessages=zero.SetMessages
                                        UserAction=zero.UserAction
                                        //AutoPlayState=zero.AutoPlayState
                                        MultiPlayerState=None;
                                        OtherGameToken=None}
        | StartedMultiGame (id,token) ->{state with 
                                            EditableMessageId=None;
                                            KeyboardMarkup=None
                                            GameId=id;
                                            SetMessages=zero.SetMessages
                                            UserAction=zero.UserAction
                                            //AutoPlayState=zero.AutoPlayState
                                            MultiPlayerState=(initMultiState token state.User)|>Some;
                                            OtherGameToken=None}
        | EndedGame -> { zero with User=state.User; GameId=state.GameId}
        | StartedNewRound -> { zero with User=state.User; GameId=state.GameId; MultiPlayerState=state.MultiPlayerState;}            
        | UpdatedUserAction  userActionState ->
            { state with UserAction=userActionState} 
        | UpdatedSetMessage (setIndex,message,keyString,setEnded) ->
            let setMessages =   if setEnded 
                                then state.SetMessages.Add(setIndex,message).Add(setIndex+1,"") 
                                else state.SetMessages.Add(setIndex,message)
            { state with 
                    SetMessages=setMessages;
                    KeyboardMarkup=keyString|>Some}
        //multi player
        | JoinedAnotherGame token ->
            { state with    OtherGameToken=token|>Some;
                            EditableMessageId=None;
                            KeyboardMarkup=None;
                            SetMessages=zero.SetMessages} 
        | JoinedMultiPlayerGame user -> 
            let gameState = state|>getMultiState
            match gameState.Players.Count with
            | 6 -> failwith "there are six players in game already!"
            | _ -> 
                let gameState' = { gameState with Players=gameState.Players.Add(user.TelegramId,user)}
                { state with MultiPlayerState=gameState'|>Some}           
        | LeftMultiPlayerGame user -> 
            let gameState = state|>getMultiState
            let gameState' = { gameState with Players=gameState.Players.Remove(user.TelegramId)}
            { state with MultiPlayerState=gameState'|>Some}   
        | UpdatedCurrentMultiplayerUser playerId ->
            updateMultiPlayerUser playerId state
        //user UI state commands
        | UpdatedEditableMessageId messageId -> 
            { state with EditableMessageId=messageId|>Some}
        | ClearedEditableMessageId ->
            { state with EditableMessageId=None}

    let exec item = 
        let apply event = 
            let newItem = apply item event
            newItem,event
        function
        | Initialize user                       -> user |> Initialized |> apply
        | StartSingleGame gameId                -> gameId |> StartedSingleGame |> apply
        | StartMultiGame (gameId,token)         -> (gameId,token) |> StartedMultiGame |> apply
        | EndGame                               -> EndedGame |> apply
        | StartNewRound                         -> StartedNewRound |> apply
        | UpdateUserAction userAction           -> userAction |> UpdatedUserAction |> apply
        | UpdateSetMessage 
            (setId,message,keyboards,setEnded)  -> (setId,message,keyboards,setEnded) |> UpdatedSetMessage |> apply
        //multi player
        | JoinAnotherGame token                 -> token |> JoinedAnotherGame |> apply
        | JoinMultiPlayerGame user              -> user |> JoinedMultiPlayerGame |> apply
        | LeaveMultiPlayerGame user             -> user |> LeftMultiPlayerGame |> apply
        | UpdateCurrentMultiplayerUser playerId -> playerId |> UpdatedCurrentMultiplayerUser |> apply
        //user UI state commands
        | UpdateEditableMessageId messageId     -> messageId |> UpdatedEditableMessageId |> apply
        | ClearEditableMessageId                -> ClearedEditableMessageId |> apply
