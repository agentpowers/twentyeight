﻿namespace TwentyEight.Game
module Round =
    open System
    open GameAttributes
    open Utilities
    open Card

    type NewRoundArgs =
        {
            RoundStartingPlayerId:System.Guid;
            RoundStartingPlayerIndex:int;
            RoundPlayers:List<Player>;
        }
    [<CLIMutable>]
    type RoundType = 
                {
                    Id:System.Guid;
                    GameId:System.Guid;
                    //IsWaitingForBids:bool;
                    //PassCount:int;
                    //ThaniMode:bool;
                    StartingPlayerId:System.Guid;
                    StartingPlayerIndex:int;
                    CurrentPlayerId:System.Guid;
                    CurrentTrickIndex:int;
                    CurrentSetIndex:int;
                    Players:Player list;
                    //ResponseMessages:List<Response>;
                    Sets:Set list;
                    Score:RoundScore option;
                    Bid:RoundBid;
                    Status: Status;
                }
    let GetNewStartingPlayerId round=
        let currIndex = round.Players |> List.findIndex (fun x -> x.PlayerId=round.StartingPlayerId)
        if (currIndex+1)>5 
            then round.Players.Item(0).PlayerId
            else round.Players.Item(currIndex+1).PlayerId

    let GetNewStartingPlayerIdAndIndex (players:Player list) currIndex=
        if (currIndex+1)>5 
            then players.Item(0).PlayerId,0
            else players.Item(currIndex+1).PlayerId,currIndex+1
    
    let GetCurrentPlayer round=
        round.Players |> List.find (fun x -> x.PlayerId=round.CurrentPlayerId)
    
    let GetPlayer round playerId=
        round.Players |> List.find (fun x -> x.PlayerId=playerId)

    let BidOwner round= 
        let getOwner bid = List.find (fun (x:Player) -> x.PlayerId=bid.BidderId) round.Players
        match round.Bid with 
        | Bid x -> x|>getOwner
        | OpenedBid (x,_) -> x|>getOwner
        | ThaniBid x -> x |> GetPlayer round
        | _ -> failwith "unable to find bid owner"
    
    let NewRound (id:System.Guid,gameId:System.Guid,newRoundArgs:NewRoundArgs) = 
            {
                Id=id;
                GameId=gameId;
                Status=Active;
                //IsWaitingForBids=false;
                //PassCount=0;
                //ThaniMode=false;
                StartingPlayerId=newRoundArgs.RoundStartingPlayerId;
                StartingPlayerIndex=newRoundArgs.RoundStartingPlayerIndex;
                CurrentPlayerId=newRoundArgs.RoundStartingPlayerId;
                CurrentTrickIndex=0;
                CurrentSetIndex=0;
                Sets=List.empty<Set>;
                Bid=WaitingForBid;
                Score=None;
                Players=newRoundArgs.RoundPlayers;
            }  
    let ShuffleCardsToPlayers (players:Player list)=
        let rand = new Random()
        let mutable i = 36
        let mutable p = 0
        let tempDeck = ResizeArray<Card>()
        tempDeck.AddRange(Deck)
        // clear out hand
        players |> Seq.iter (fun g -> g.Hand <- List.empty)
        while i>0 do
            let randNum = rand.Next(0,tempDeck.Count-1)
            let card = tempDeck.Item(randNum)
            match p with 
            | 0 |1 | 2| 3 | 4| 5  -> players.Item(p).Hand <-  List.append (players.Item(p).Hand) ([card])
            | _-> ()
            tempDeck.RemoveAt(randNum)
            i <- i-1
            match p with
            | 5 -> p <- 0
            | _ -> p <- p+1
        for player in players do 
            player.Hand <- player.Hand |> List.sortBy (fun g -> g.Suit, g.Order)
        players

    let GetShuffledCards round=
        match round.Bid with 
        | WaitingForBid -> 
                    let players = ShuffleCardsToPlayers round.Players
                    {round with Players = players}
        | _ -> round

