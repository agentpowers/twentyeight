﻿namespace TwentyEight.Game
module GameActions=
    open Round
    open Utilities
    open Card
    open GameAttributes
    
    let GetNextPlayer round = 
        let currentPlayerIndex = round.Players |> Seq.findIndex ( fun g-> g.PlayerId=round.CurrentPlayerId)
        let getNextIndex =match currentPlayerIndex+1 with
                            | 6 -> 0
                            | x -> x
        match round.Bid with 
        | ThaniBid _-> 
            let bidder =Round.GetPlayer round (round.Bid|>GetThaniPlayerId)
            let skipCount = match bidder.TeamId=(Round.GetCurrentPlayer round).TeamId  with
                            | true -> 0
                            | false -> 1
            round.Players.Item(getNextIndex+skipCount)
        | _ -> round.Players.Item(getNextIndex)
    let PostThaniAction (round:RoundType)= 
        let currentPlayer = Round.GetCurrentPlayer round
        //TODO: make Thurupu optional
//        let bid= {Bid=0;BidderId=round.CurrentPlayerId;Thurupu={Suit=Dice;Rank=Jack}; OpenedPlayerId=None;OpenedSetId=None;OpenedTrickId=None;OpenedAt=None}
        {round with  CurrentPlayerId=currentPlayer.PlayerId; Bid=currentPlayer.PlayerId |> ThaniBid;}
    let PostPassAction round= 
        let passCount,bid = match round.Bid with
                            | ProcessingBid (a,b)-> (a,b)
                            | _ -> failwith "invalid round bid"
        match passCount+1 with
        | 5 ->  
                let bid,_ = GameAttributes.GetBid round.Bid
                {round with  Bid = RoundBid.Bid bid; CurrentPlayerId=round.StartingPlayerId; }
        | _ ->  {round with  Bid = RoundBid.ProcessingBid (passCount+1,bid); CurrentPlayerId=(round|>GetNextPlayer).PlayerId;}
    
    let PostBidAction bid (round:RoundType)=
        let bid= {Value=bid.Value;BidderId=round.CurrentPlayerId;Thurupu=bid.Thurupu;}
        let passCount = 0;
        {round with  Bid=ProcessingBid (passCount,bid); CurrentPlayerId=(round|>GetNextPlayer).PlayerId;}
    
    let PostOpenThurupuAction (round:RoundType)=
        let bid,_ = GameAttributes.GetBid round.Bid
        let openedInfo ={OpenedPlayerId=round.CurrentPlayerId;OpenedSetId=round.CurrentSetIndex;OpenedTrickId=round.CurrentTrickIndex;OpenedAt=System.DateTime.Now}
        let bid= OpenedBid (bid,openedInfo)//{round.Bid.Value with OpenedPlayerId=Some round.CurrentPlayerId;OpenedSetId=Some round.CurrentSetIndex;OpenedTrickId=Some round.CurrentTrickIndex;OpenedAt=Some System.DateTime.Now}
        {round with Bid=bid;}
   

