﻿namespace TwentyEight.Game
module ValidationAndCheating=
    open Round
    open GameAttributes
    open Utilities
    open Card

    let suitCountInHand suit hand = hand |> Seq.filter (fun g->g.Suit=suit) |> Seq.length

    let validateCardInHand card round = 
        match Seq.contains card (round|>GetCurrentPlayer).Hand with
        | true -> Valid
        | false -> Invalid "invalid card"

    let ValidateActiveRound round =
        match round.Status with 
            | Ended -> Invalid "game already over! Start a new ROUND"
            | Active -> Valid

    let ValidateRoundEnded round =
        match round.Status with 
            | Ended -> Valid
            | Active -> Invalid "active game"

    let ValidateCurrentPlayer playerId round =
        match playerId=round.CurrentPlayerId with
        | true -> Valid
        | false -> Invalid "Not ur turn"

    let ValidatePlayer playerId round =
        match round.Players |> Seq.exists (fun i -> i.PlayerId = playerId) with
        | true -> Valid
        | false -> Invalid "invalid player"
    
    let ValidateBid currentBidValue roundBid=
        let validateExistingBid bid passCount= 
            match currentBidValue with
            | currentBid when currentBid>=16 && currentBid<=bid.Value -> Invalid (sprintf "Bid must be greater than %d" currentBid)
            | _ -> match (passCount % 2) with
                    | 0 ->Valid
                    | _ -> match bid.Value with
                            | g when g <20 && currentBidValue <20 -> Invalid "You must post a bid bigger than 19"
                            | g when g <24 && g >=20 && currentBidValue < 24 -> Invalid "You must post a bid bigger than 23"
                            | g when g <=28 && g >24 && currentBidValue <28 -> Invalid "You must post 28"
                            | _ -> Valid
        match currentBidValue with
                    | x when x<16 -> Invalid "Bid must be greater than 16"
                    | x when x>28 -> Invalid "Bid must be less than 28"
                    | _ -> match roundBid with
                            | WaitingForBid -> Valid
                            | ProcessingBid (passCount,x) -> validateExistingBid x passCount
                            | Bid _ -> Invalid "Bid already completed"
                            | OpenedBid (_,_) -> Invalid "Bid already opened"
                            | ThaniBid _-> Invalid "Thani Bid"
    
    let ValidatePass playerId round =
        match round.Bid with 
        | ProcessingBid (_,_) -> 
            match round.StartingPlayerId=playerId,round.Bid with
                    | true,bid -> match bid with 
                                    | WaitingForBid ->  Invalid "You are the first player, cannot post pass"
                                    | _ ->Valid
                    | _ -> Valid 
        | _ -> Invalid "Game already started! why are you passing?"
    
    let ValidateShowThurupu playerId round hasSuit=
        let bid,isBidShown= GameAttributes.GetBid round.Bid
        match round.Bid with
        | WaitingForBid | ProcessingBid (_,_)-> Invalid "No bids yet"
        | _ -> match isBidShown with 
                    | true -> Invalid "thurupu already open"
                    | _ -> match round.CurrentTrickIndex with 
                                            | 0 -> Invalid "nothing to cut"
                                            | _ -> match round.Sets.Head.Tricks|> List.tryLast with 
                                                    | Some x when (hasSuit (GetCurrentPlayer round).Hand x.PlayedCard.Suit) -> Invalid "Can't open thurupu when suit is in hand"
                                                    | Some x when playerId=bid.BidderId && x.PlayedCard.Suit=bid.Thurupu.Suit && ((GetCurrentPlayer round).Hand |> suitCountInHand bid.Thurupu.Suit)=1 -> Valid
                                                    | _ -> Valid
    
    let ValidateThaniCall playerId round =
        match round.Bid with 
        | WaitingForBid 
        | ProcessingBid _ -> Valid
        | _ -> Invalid "Game already started!"

    let CheckCheating card (round:RoundType) =
        let currentPlayer = GetCurrentPlayer round
        let cardInHand() = Seq.contains card currentPlayer.Hand
        let startingTrick()=round.Sets.Head.Tricks|> Seq.last
        let suitInPlay() = startingTrick().PlayedCard.Suit
        let playedAnotherSuitWhenHasThatInHand() = 
            match (suitInPlay()<>card.Suit && (currentPlayer.Hand|> Seq.exists (fun g->g.Suit=suitInPlay()))) with 
            | true -> Some Player_played_a_non_current_suit_when_has_it_in_hand
            | false -> None 
        let justOpenedThurupu() = 
            match round.Bid with
            | OpenedBid (a,b)-> b.OpenedPlayerId=round.CurrentPlayerId && b.OpenedSetId=round.CurrentSetIndex && b.OpenedTrickId=round.CurrentTrickIndex
            | _ -> false
//            bidIsShown && bid.OpenedPlayerId.Value=round.CurrentPlayerId && bid.OpenedSetId.Value=round.CurrentSetIndex && bid.OpenedTrickId.Value=round.CurrentTrickIndex
        let didNotPlayThurupu bid = bid.Thurupu.Suit<>card.Suit 
        let onlyThurupuInHand bid = currentPlayer.Hand|> Seq.forall ( fun g->g.Suit=bid.Thurupu.Suit)
        let playedThurupuSuit bid = bid.Thurupu.Suit=card.Suit
        let isThurupu bid = bid.Thurupu.Suit=card.Suit && bid.Thurupu.Rank=card.Rank
        let isThurupuSuit bid = bid.Thurupu.Suit=card.Suit
//        match cardInHand() with
//        | true ->
//            match (round.CurrentSetIndex,round.CurrentTrickIndex) with
//                | (0,0)-> match (isBidder && bid.Thurupu.Suit=card.Suit) with
//                            | (true) -> match isThurupu() with
//                                            | true ->Some Bidder_played_thurupu_on_first_play
//                                            | false ->  match onlyThurupuInHand() with
//                                                        |false -> Some Bidder_played_thurupu_when_other_suit_in_hand
//                                                        |true -> None
//                            | _->None
//                | (_,0)-> match (isBidder,bid.IsShown,onlyThurupuInHand()) with
//                            | (true,false,false) -> match (playedThurupuSuit()) with 
//                                                    | true -> Some Bidder_played_thurupu_when_other_suit_in_hand
//                                                    | false -> None
//                            | (true,false,true) -> match (isThurupu(),(currentPlayer.Hand|>Seq.length)=1) with
//                                                    | true,false-> Some Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open
//                                                    | _ -> None
//                            | _ -> None
//                | (_,_) -> match justOpenedThurupu() with 
//                            | true -> match ( card.Suit<>bid.Thurupu.Suit && currentPlayer.Hand|> Seq.exists (fun g->g.Suit=bid.Thurupu.Suit)) with
//                                        | true -> Some Player_just_opened_thurupu_and_didnot_play_thurupu_when_it_is_in_hand
//                                        | false -> None
//                            | _->match playedAnotherSuitWhenHasThatInHand() with 
//                                   | true -> Some Player_played_a_non_current_suit_when_has_it_in_hand
//                                   | false -> None 
//          | false -> Some Player_played_card_not_in_hand
        let checkCheating() =
            let bid,isBidShown =  GetBid round.Bid
            let isBidder = bid.BidderId=round.CurrentPlayerId
            match (round.CurrentSetIndex,round.CurrentTrickIndex,justOpenedThurupu()) with
            | (setIndex,trickIndex,false) 
                        when setIndex=0 && trickIndex=0 && isBidder && bid.Thurupu.Suit=card.Suit->
                            match isThurupu bid with
                            | true ->Some Bidder_played_thurupu_on_first_play
                            | false ->  match onlyThurupuInHand bid with
                                        |false -> Some Bidder_played_thurupu_when_other_suit_in_hand
                                        |true -> None
            | (_,0,false)-> match (isBidder,isBidShown,bid|>onlyThurupuInHand) with
                            | (true,false,false) -> match (playedThurupuSuit bid) with 
                                                    | true -> Some Bidder_played_thurupu_when_other_suit_in_hand
                                                    | false -> None
                            | (true,false,true) -> match (isThurupu bid,(currentPlayer.Hand|>Seq.length)=1) with
                                                    | true,false-> Some Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open
                                                    | _ -> None
                            | _ -> None
            | (_,_,false)->
                            match (isBidder,isBidShown,onlyThurupuInHand bid) with
                            | (true,false,false) -> match bid|>isThurupu with
                                                    | true -> Some Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open
                                                    | _ ->
                                                            match (bid|>isThurupuSuit && suitInPlay()<>card.Suit) with 
                                                            | true -> Some Bidder_played_thurupu_when_other_suit_in_hand
                                                            | false -> 
                                                                match currentPlayer.Hand|> Seq.filter (fun g-> g.Suit=bid.Thurupu.Suit) |> Seq.length with
                                                                | 1 -> None
                                                                | _ -> playedAnotherSuitWhenHasThatInHand()
                            | (true,false,true) -> match (isThurupu bid,(currentPlayer.Hand|>Seq.length)=1) with
                                                    | true,false-> Some Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open
                                                    | _ -> None
                            | _ -> playedAnotherSuitWhenHasThatInHand()
            | (_,_,true) -> match ( card.Suit<>bid.Thurupu.Suit && currentPlayer.Hand|> Seq.exists (fun g->g.Suit=bid.Thurupu.Suit)) with
                                        | true -> Some Player_just_opened_thurupu_and_didnot_play_thurupu_when_it_is_in_hand
                                        | false -> None
        let checkThaniCheating() = 
            match (round.CurrentSetIndex,round.CurrentTrickIndex) with
            | (_,0) -> None
            | _ ->  playedAnotherSuitWhenHasThatInHand()
        match cardInHand() with
        | true->
             match round.Bid with
                | ThaniBid _-> checkThaniCheating()
                | _ ->checkCheating()
        | false -> Some Player_played_card_not_in_hand
       

