﻿namespace TwentyEight.Game
module GameAttributes=
    open Card

    type Status = 
    | Active
    | Ended 
    type Bid= {
        Value:int;
        Thurupu:Card;
        BidderId:System.Guid;
    }
    type OpenedBidInfo={
        OpenedPlayerId:System.Guid;
        OpenedSetId:int;
        OpenedTrickId:int;
        OpenedAt:System.DateTime;
    } 
    type RoundBid=
    | WaitingForBid
    | ProcessingBid of int*Bid
    | ThaniBid of System.Guid
    | Bid of Bid
    | OpenedBid of Bid*OpenedBidInfo
    
    //return tuple of bid and isbidshown
    let GetBid roundBid = 
        match roundBid with
        | Bid bid -> bid,false
        | OpenedBid (bid,_)-> bid,true 
        | ProcessingBid (_,bid) -> bid,false
        | _ -> failwith "no bid"
    
    let GetOpenedBid roundBid = 
        match roundBid with
        | Bid bid -> failwith "bid not open"
        | OpenedBid (bid,openedBidInfo) -> (bid,openedBidInfo)  
        | _ -> failwith "no bid"

    let GetThaniPlayerId roundBid =
        match roundBid with
        | ThaniBid userId -> userId
        | _ -> failwith "not thani"
        
    let IsBidShown roundBid = 
        match roundBid with
        | WaitingForBid | ProcessingBid (_) ->false
        | _ ->true    
     
    type Trick={ PlayerId:System.Guid;PlayedCard:Card; PlayedAt:System.DateTime;}

    type CheatTypes = 
        |Bidder_played_thurupu_on_first_play 
        |Bidder_played_thurupu_when_other_suit_in_hand 
        |Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open
        |Player_played_a_non_current_suit_when_has_it_in_hand 
        |Player_just_opened_thurupu_and_didnot_play_thurupu_when_it_is_in_hand 
        |Player_just_opened_thurupu_while_having_current_suit_in_hand 
        |Player_played_card_not_in_hand

    let GetCheatString cheatType =
        match cheatType with 
        |Bidder_played_thurupu_on_first_play -> "cannot play thurupu on first play"
        |Bidder_played_thurupu_when_other_suit_in_hand -> "cannot play thurupu when other suit in hand"
        |Bidder_played_thurupu_card_when_other_thurupu_in_hand_and_when_not_open -> "cannot play thurupu card when not open"
        |Player_played_a_non_current_suit_when_has_it_in_hand -> "must play suit when in hand"
        |Player_just_opened_thurupu_and_didnot_play_thurupu_when_it_is_in_hand -> "must play thurupu after opening thurupu"
        |Player_just_opened_thurupu_while_having_current_suit_in_hand -> "cannot open thurupu when suit is in hand"
        |Player_played_card_not_in_hand -> "played card not in hand"

    type Cheat={ CommitedBy:int;TrickIndex:int;SetIndex:int;Details:string;CheatId:System.Guid;CheatType:CheatTypes}

    type CheatChallenge = {Accuser:int;Accused:int;CheatType:CheatTypes;When:System.DateTime;Success:bool;CheatId:System.Guid}

    type Set = {
                    SetId:System.Guid;
                    Tricks:List<Trick>;
                    Winner:System.Guid option;
                    IsCut:bool option;
                    CutBy:Card option;
                    Cheats:List<Cheat> option;
                    CheatChallenges:List<CheatChallenge> option;
                    CompletedAt:System.DateTime option;
               }
               static member DefaultSet trick =
                {SetId=System.Guid.NewGuid();Tricks=[trick];Winner=None;IsCut=None;CutBy=None;Cheats=None;CheatChallenges=None;CompletedAt=None;}

    type TeamBidType=
    | Honors
    | Typical
    | TwentyEight
    | Thani
    
    type TeamWinnerRoutes =
    | Team1BidAndWon
    | Team1BidAndLost
    | Team2BidAndWon
    | Team2BidAndLost 

    type TeamWinnerType = TeamWinnerRoutes*TeamBidType

    type ReasonUnableToCut =
        | HasSuit
        | NoThurupu
    
    type ThurupuOptions =
        | Under
        | Over
        | CantCut of ReasonUnableToCut
        | First
        | OpenThurupuButCantCut
    
    type PlayerRoute =
        | ThaniPlayer
        | FirstPlayerOfRound
        | FirstPlayerOfRoundAndBidder
        | FirstPlayerOfRoundAndBidderAndAllThurupu
        | FirstPlayerOfSet
        | FirstPlayerOfSetAndCanCut
        | FirstPlayerOfSetAndBidder
        | FirstPlayerOfSetAndBidderAndAllThurupu
        | PlayerAbleToCutWithTeamWinning
        | PlayerAbleToUnderCutWithTeamWinning
        | PlayerAbleToOverCutWithTeamWinning
        | PlayerWithTeamWinning of ReasonUnableToCut
        | PlayerAbleToCutWithOpponentWinning
        | PlayerAbleToUnderCutWithOpponentWinning
        | PlayerAbleToOverCutWithOpponentWinning
        | PlayerWithOpponentWinning of ReasonUnableToCut
        | OpenThurupuButUnableToCutWithTeamWinning
        | OpenThurupuButUnableToCutdWithOpponentWinning