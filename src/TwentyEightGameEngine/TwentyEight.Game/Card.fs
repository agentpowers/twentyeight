﻿namespace TwentyEight.Game
module Card =
    open System
    //open Microsoft.FSharp.Reflection

    type Rank =
    |Jack
    |Nine
    |Ace
    |Ten
    |King
    |Queen
    |Eight
    |Seven
    |Six

    let matchRank rankString=
        match rankString with
        |"Jack" -> Some Jack
        |"Nine" -> Some Nine
        |"Ace" -> Some Ace
        |"Ten" -> Some Ten
        |"King" -> Some King
        |"Queen"  -> Some Queen
        |"Eight" -> Some Eight
        |"Seven" -> Some Seven
        |"Six" -> Some Six
        | _ -> None

    type Suit = 
    |Spade
    |Dice
    |Heart
    |Clubs

    let matchSuit suitString =
        match suitString with
        | "Spade" -> Some Spade
        | "Dice" -> Some Dice
        | "Heart" -> Some Heart
        | "Clubs" -> Some Clubs
        | _ -> None

    [<CLIMutable>]
    type Card = 
        {
            Suit:Suit;
            Rank:Rank
        }
        member this.Points= 
                    match this.Rank with 
                    | Jack -> 3
                    | Nine -> 2
                    | Ace -> 1
                    | Ten -> 1
                    | _ -> 0
        member this.Order= 
                    match this.Rank with 
                    |Jack->1 |Nine->2 |Ace->3 |Ten->4 |King->5 |Queen->6 |Eight->7 |Seven->8 |Six->9 
    
    let HasSuit cards suit = cards |> Seq.exists (fun g->g.Suit=suit)
    
    let Deck = [
                    // All S
                    {Suit=Spade;Rank=Jack};
                    {Suit=Spade;Rank=Nine};
                    {Suit=Spade;Rank=Ace};
                    {Suit=Spade;Rank=Ten};
                    {Suit=Spade;Rank=King};
                    {Suit=Spade;Rank=Queen};
                    {Suit=Spade;Rank=Eight};
                    {Suit=Spade;Rank=Seven};
                    {Suit=Spade;Rank=Six};
                    // All Ds
                    {Suit=Dice;Rank=Jack};
                    {Suit=Dice;Rank=Nine};
                    {Suit=Dice;Rank=Ace};
                    {Suit=Dice;Rank=Ten};
                    {Suit=Dice;Rank=King};
                    {Suit=Dice;Rank=Queen};
                    {Suit=Dice;Rank=Eight};
                    {Suit=Dice;Rank=Seven};
                    {Suit=Dice;Rank=Six};
                        // All Cs
                    {Suit=Clubs;Rank=Jack};
                    {Suit=Clubs;Rank=Nine};
                    {Suit=Clubs;Rank=Ace};
                    {Suit=Clubs;Rank=Ten};
                    {Suit=Clubs;Rank=King};
                    {Suit=Clubs;Rank=Queen};
                    {Suit=Clubs;Rank=Eight};
                    {Suit=Clubs;Rank=Seven};
                    {Suit=Clubs;Rank=Six};
                        // All Ds
                    {Suit=Heart;Rank=Jack};
                    {Suit=Heart;Rank=Nine};
                    {Suit=Heart;Rank=Ace};
                    {Suit=Heart;Rank=Ten};
                    {Suit=Heart;Rank=King};
                    {Suit=Heart;Rank=Queen};
                    {Suit=Heart;Rank=Eight};
                    {Suit=Heart;Rank=Seven};
                    {Suit=Heart;Rank=Six};
                ]
    
    let ShuffledDeck = 
                let removeItem (currentCards:List<Card>, item:Card) =
                    List.filter(fun (i:Card)->i<>item) currentCards
                let rec addRandomToDeck (currentCards:List<Card>,remaining:List<Card>)=
                    match remaining with
                    | [] -> currentCards
                    | _  -> let randItem = remaining.Item(System.Random().Next(0,remaining.Length))
                            addRandomToDeck(randItem :: currentCards,removeItem(remaining,randItem))
                        
                addRandomToDeck([],Deck)
    
    let CardsGroupedBySuit hand= Seq.groupBy (fun g->g.Suit) hand
    
    let CardsSumAndPointsBySuit hand=   CardsGroupedBySuit hand
                                        // returns Tuple (suit, cards, number of cards, sum of card points)
                                                |> Seq.map((fun (key,values) -> (key,values, Seq.length(values),Seq.sumBy (fun (g:Card)->g.Points) values)))
    
    let PotentialThurupu hand=  CardsSumAndPointsBySuit hand
                                        |> Seq.maxBy (fun (_,_,n,p) -> n+p) // get max by number of cards + sum of points
                                        |> fun (_,c,_,_) -> c // return cards of max
                                        |> Seq.maxBy(fun (g:Card)->g.Order) // max by order

    let PotentialBid hand=
        let mutable bid=0.0
        let avgHandPoints=28.0/6.0 // place to increase or decrease AI bidding agression
        let thurupuCard = PotentialThurupu hand
        let allNonThurupuJacksCount = hand |> Seq.filter (fun g-> g.Rank = Jack && g.Suit <> thurupuCard.Suit) |> Seq.length
        // avgHandPoints given to all non thurupus jack -> high probability of guarentee hand
        bid <- System.Convert.ToDouble allNonThurupuJacksCount * avgHandPoints
        let cardsSumAndPointsBySuit=CardsSumAndPointsBySuit hand
        let suitsNotInHandCount= 4 - (cardsSumAndPointsBySuit |> Seq.length)
        let allThurupuCards= hand |> Seq.filter (fun g->g.Suit = thurupuCard.Suit)
        let mutable thurupuInHandCount=Seq.length allThurupuCards
        //one avgHandPoints is given to each suit not in hand and there is a thurupu available to cut
        let getSuitNotInHandMultiplier =    match (suitsNotInHandCount,thurupuInHandCount) with
                                            | (x,y) when x=0 && y=0 ->0
                                            | (x,y) when x=y ->x
                                            | (x,y) when x>y -> y
                                            | (x,y) when y>x -> x
                                            | _ -> 0
        bid <- (bid + (System.Convert.ToDouble getSuitNotInHandMultiplier * avgHandPoints))
        thurupuInHandCount <- (thurupuInHandCount-getSuitNotInHandMultiplier)
        //for each thurupu leftover with points give a hand 
        // cards are already ordered by Rank
        let leftoverThurupCardsWithPoints = allThurupuCards |> Seq.take thurupuInHandCount |> Seq.filter (fun g->g.Points>0) |> Seq.length
        bid+(System.Convert.ToDouble leftoverThurupCardsWithPoints*avgHandPoints)


