﻿namespace TwentyEight.Game
module Utilities=
    open System
    //open Microsoft.FSharp.Reflection

    type ResponseType = Public|Private|InGame
    
    type ResponseCode = Failed=0|Success=1

    type Validity =
        | Invalid of string
        | Valid 
    
    type Response = 
                {
                    ResponseCode:ResponseCode;
                    Message:string;
                    Created:System.DateTime;
                    CreatedBy:Guid;
                    ResponseType:ResponseType
                }
    
    type RoundScore= 
                {
                    WinnerTeamId:int;
                    FirstTeamPoints:int;
                    SecondTeamPoints:int;
                    Message:string;
                }
    

