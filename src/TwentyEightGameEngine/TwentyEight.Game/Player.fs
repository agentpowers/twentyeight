﻿namespace TwentyEight.Game
open System
open Card
open GameAttributes

type Player = 
            {
                Name:string;
                PlayerId:System.Guid;
                TeamId:int;
                //Bidder:bool;
                mutable Hand: Card list;
                Ready:bool;
                Kunukus:int;
                Ai :bool;
            }
            member this.RemoveCardFromHand card = 
                this.Hand <-this.Hand |> Seq.where (fun g->g<>card)|>Seq.toList
            member this.IsBidder bidderId =
                this.PlayerId=bidderId
            member this.IsCurrentPlayer playerId =
                this.PlayerId=playerId
            member this.Thurupu() = match this.Hand|> Seq.length with 
                                    | 6 -> PotentialThurupu this.Hand
                                    | _-> failwith "6 cards needed when getting potential thurupu"
            member this.Bid() = match this.Hand|> Seq.length with 
                                    | 6 -> PotentialBid this.Hand
                                    | _-> 0.0
            member this.HasKunuku = this.Kunukus>0;
            static member DefaultPlayerNames = ["A";"X";"B";"Y";"C";"Z"]
            static member PopulateWebMultiPlayers (players: (Guid*string) array)=
                let player index isAi name=
                    if isAi 
                    then Guid.NewGuid(),name
                    else 
                        players.[index]

                List.mapi (fun i (a) ->
                           let isAi = (Array.length players) <=  i
                           let id,name= player i isAi a
                           { 
                                Name= name;
                                PlayerId=id;
                                TeamId=if i%2=0 then 1 else 2
                                //Bidder=false;
                                Hand = List.empty;
                                Ready=false;
                                Kunukus=0;
                                Ai=isAi;
                            }) Player.DefaultPlayerNames

            static member PopulateMultiPlayers (players: Guid list) (playerNames:string list)=
                let id index isAi=
                    if isAi 
                    then Guid.NewGuid() 
                    else 
                        players.[index]

                List.mapi (fun i (a) ->
                           let isAi = (List.length players) <=  i
                           let id= id i isAi
                           { 
                                Name= playerNames.[i];
                                PlayerId=id;
                                TeamId=if i%2=0 then 1 else 2
                                //Bidder=false;
                                Hand = List.empty;
                                Ready=false;
                                Kunukus=0;
                                Ai=isAi;
                            }) playerNames
  
            static member PopulatePlayers (playerNames:string list) id =
                let player = { 
                                Name=playerNames.[0];
                                PlayerId=id;
                                TeamId=1
                                //Bidder=false;
                                Hand = List.empty;
                                Ready=false;
                                Kunukus=0;
                                Ai=false;
                            }
                let fakePlayers = playerNames   |> List.tail
                                                |> List.mapi (fun i name -> 
                                                { 
                                                    Name=name;
                                                    PlayerId=System.Guid.NewGuid();
                                                    TeamId=if i%2=0 then 2 else 1
                                                    //Bidder=false;
                                                    Hand = List.empty;
                                                    Ready=false;
                                                    Kunukus=0;
                                                    Ai=true;
                                                }) 
                (player):: fakePlayers
  
            static member GetFakePlayers numberOfFakePlayers index=
                let playerRange= [index..numberOfFakePlayers]
                playerRange |> List.map (fun x -> 
                                                { 
                                                    Name=sprintf "Player%i" x;
                                                    PlayerId=System.Guid.NewGuid();
                                                    TeamId=if x%2=0 then 2 else 1
                                                    //Bidder=false;
                                                    Hand = List.empty;
                                                    Ready=false;
                                                    Kunukus=0;
                                                    Ai=true;
                                                }) 

 
type Team ={ Points:int;TeamId:int;Name:string;}

type PlayerBidActions=
                | PostBid of Bid
                | Pass

type PlayerActions=
            | Bid of PlayerBidActions
            | Play of Card
            | Cut of Card
            | ShowThurupu

type UiPlayerAction = 
    | UiBid of Bid
    | UiPass
    | UiThani
    | UiShowThurupu
    | UiPlay of Card
