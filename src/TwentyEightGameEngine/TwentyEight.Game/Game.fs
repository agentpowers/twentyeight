﻿namespace TwentyEight.Game
module SinglePlayer=
    open System
    open Microsoft.FSharp.Reflection
    open Utilities
    open Card 
    open GameAttributes
    open Round
    open ValidationAndCheating
    open GameActions
    
    [<CLIMutable>]  
    type CreateGameArgs={GameId:Guid;RoundId:Guid;UserId:Guid; Team1Name:string; Team2Name:string; Players:Player list; CreatedByUser:System.Guid;CreatedTime:DateTime}

    [<CLIMutable>]     
    type Game = 
            {
                Id:System.Guid;
                Team1:Team;
                Team2:Team;
                CurrRound:RoundType;
                CurrRoundId:System.Guid;
                ValidateCheating:bool;
                Message:Response;
                Status:Status
            }
            member this.MoveToNextRound newRoundArgs=
                let currentRound= this.CurrRound
                match currentRound |> ValidateRoundEnded with
                | Invalid err -> failwith err
                | Valid ->  let newRound = Round.NewRound(System.Guid.NewGuid(),this.Id,newRoundArgs)
                            { this with CurrRound={ newRound with Score=currentRound.Score};CurrRoundId=newRound.Id}     
            member this.RestartRound newRoundArgs=
                let newRound =Round.NewRound(this.CurrRoundId,this.Id,newRoundArgs)
                { this with CurrRound={ newRound with Score=this.CurrRound.Score} }
            member this.NewRound newRoundArgs =
                let newRound =Round.NewRound(this.CurrRoundId,this.Id,newRoundArgs)
                                |> GetShuffledCards
                { this with CurrRound={ newRound with Score=this.CurrRound.Score} }
            member this.EndGame() =
                {this with Status = Ended}
            static member GetEmptyGame()= 
                {
                    Id=Guid.Empty;
                    Team1={Points=0;TeamId=1;Name="1"};
                    Team2={Points=0;TeamId=1;Name="Team1";};
                    CurrRound = Round.NewRound(Guid.Empty,Guid.Empty,{RoundStartingPlayerId = Guid.Empty; RoundStartingPlayerIndex=0; RoundPlayers=List.empty});
                    CurrRoundId=Guid.Empty;
                    ValidateCheating=true;
                    Status=Active;
                    Message=
                            {
                                ResponseCode=ResponseCode.Success;
                                Message="Game Started";
                                Created=DateTime.Now;
                                CreatedBy=Guid.Empty;
                                ResponseType=ResponseType.Public;
                            }
                }
            static member NewGame(newGame:CreateGameArgs) = 
                    let newRoundArgs = {RoundStartingPlayerIndex=0;RoundStartingPlayerId=newGame.Players.Item(0).PlayerId;RoundPlayers=newGame.Players}
                    let newRound =Round.NewRound(newGame.RoundId,newGame.GameId,{RoundStartingPlayerIndex=0;RoundStartingPlayerId=newGame.Players.Item(0).PlayerId;RoundPlayers=newGame.Players})
                    {
                        Id=newGame.GameId;
                        Team1={Points=0;TeamId=1;Name=newGame.Team1Name;};
                        Team2={Points=0;TeamId=2;Name=newGame.Team2Name;};
                        CurrRound=newRound
                        CurrRoundId=newRound.Id;
                        ValidateCheating=false;
                        Status=Active;
                        Message=
                            {
                                ResponseCode=ResponseCode.Success;
                                Message="Game Started";
                                Created=newGame.CreatedTime;
                                CreatedBy=newGame.CreatedByUser;
                                ResponseType=ResponseType.Public;
                            }
                    }


    let allNonThurupCards hand thurupu = hand |> Seq.filter (fun g->g.Suit <> thurupu.Suit)
    let allThurupCards hand thurupu= hand |> Seq.filter (fun g->g.Suit = thurupu.Suit)
    let private getPlayerRoutes (round:RoundType)=
        let player= GetCurrentPlayer round
        let sets = round.Sets
        let players= round.Players
        let setAlreadyBeenCut() = sets.Head.IsCut
        let currentCutCard() = sets.Head.CutBy.Value
        let sameTeamIsTheWinner() = players |> Seq.find (fun g->g.PlayerId=sets.Head.Winner.Value) |> fun g->g.TeamId=player.TeamId
        let canOverCut (allThurupCards:seq<Card>) = allThurupCards |> Seq.exists ( fun g->g.Order < currentCutCard().Order) 
        let playerHasSuit suit = player.Hand |> Seq.exists (fun g->g.Suit=suit)
        let getPlayerCutType bid = 
            let allThurupCards=allThurupCards player.Hand bid.Thurupu
            match playerHasSuit ((sets.Head.Tricks|>Seq.last).PlayedCard.Suit) with
                                    | true -> CantCut HasSuit// has suit so player can't cut
                                    | false -> match (setAlreadyBeenCut().IsSome,(Seq.length allThurupCards)>0) with // (alreadycut,hasThurupu)
                                                | (false,true) -> First // (hasn't been cut before/has thurupus) -> so player able to cut 
                                                | (true,true) -> match (canOverCut allThurupCards) with // (alreadybeencut/hasthurupus) -> player can cut if has higher cut card
                                                                    | true -> Over
                                                                    | false -> Under
                                                | (false,false) -> OpenThurupuButCantCut
                                                | (true,false) -> CantCut NoThurupu// nothurupus -> can't cut // nothurupus -> can't cut
        let isFirstTrick = round.CurrentTrickIndex=0
        match round.Bid with
        | ThaniBid _ -> ThaniPlayer
        | _ ->
            let bid,isBidShown = GameAttributes.GetBid round.Bid
            let allNonThurupCards = allNonThurupCards player.Hand bid.Thurupu
            match sets with 
                        //first player
                        | []  -> match player.IsBidder bid.BidderId with // player is bidder
                                    |true -> match Seq.length allNonThurupCards with
                                                | 0 -> FirstPlayerOfRoundAndBidderAndAllThurupu  // Only thurupu -> play the best card except thurupu
                                                | _ -> FirstPlayerOfRoundAndBidder
                                    |false -> FirstPlayerOfRound
                        | _ -> match isFirstTrick with 
                                | true -> match (player.IsBidder bid.BidderId,GameAttributes.IsBidShown round.Bid) with 
                                            | (true,true) | (false,true) -> match Seq.length allNonThurupCards with 
                                                                            | 0 -> FirstPlayerOfSetAndCanCut
                                                                            | _ -> FirstPlayerOfSet
                                            | (true,false) -> match Seq.length allNonThurupCards with 
                                                                | 0 -> FirstPlayerOfSetAndBidderAndAllThurupu
                                                                | _ -> FirstPlayerOfSetAndBidder
                                            | (false,false) -> FirstPlayerOfSet
                                | false -> match (sameTeamIsTheWinner(),getPlayerCutType bid) with 
                                            | (true,CantCut reason)  -> PlayerWithTeamWinning reason
                                            | (true,First) -> PlayerAbleToCutWithTeamWinning 
                                            | (true,Under) -> PlayerAbleToUnderCutWithTeamWinning 
                                            | (true,Over) -> PlayerAbleToOverCutWithTeamWinning 
                                            | (false,CantCut reason) -> PlayerWithOpponentWinning reason
                                            | (false,First) -> PlayerAbleToCutWithOpponentWinning
                                            | (false,Under) -> PlayerAbleToUnderCutWithOpponentWinning
                                            | (false,Over) -> PlayerAbleToOverCutWithOpponentWinning
                                            | (true,OpenThurupuButCantCut) -> OpenThurupuButUnableToCutWithTeamWinning
                                            | (false,OpenThurupuButCantCut) ->OpenThurupuButUnableToCutdWithOpponentWinning

    let private startingTrick round=round.Sets.Head.Tricks|> Seq.last
    let private getTeamPlayers (players:seq<Player>) teamId = players |> Seq.where (fun g->g.TeamId=teamId) |> Seq.map (fun g-> g.PlayerId)
    let private getNext (round:RoundType)= 
        let playerRoute = round |> getPlayerRoutes
        let player= round|>GetCurrentPlayer
        let sets = round.Sets
        let players= round.Players
        let suitInPlay()=(sets.Head.Tricks|>Seq.last).PlayedCard.Suit
        let allForSuit suit = player.Hand |> Seq.filter (fun g->g.Suit = suit)
        let allExceptJacks = player.Hand |> Seq.filter (fun g->g.Rank<>Jack ) 
        let playFirstJack cards = cards |> Seq.tryFind (fun g->g.Rank=Jack)
        let getallNonThurupCards bid= allNonThurupCards player.Hand bid.Thurupu
        let getLowestCardForSuit suit = player.Hand |> Seq.filter (fun g->g.Suit=suit) |> Seq.sortBy (fun g-> -g.Order) |> Seq.item 0
        let lowestCardFromLowestSuit cards = (CardsGroupedBySuit cards)|> Seq.minBy (fun (g,h)->Seq.length h) |> (fun (g,h)->Seq.maxBy (fun (i:Card)->i.Order) h)
        let firstJackOrLowestFromSuit suit = match (allForSuit suit|>playFirstJack) with
                                                    | Some card -> card
                                                    | None -> getLowestCardForSuit suit
        let firstJackOrLowestFromLowestSuit cards = match (playFirstJack cards) with
                                                        | Some card -> card
                                                        | None -> lowestCardFromLowestSuit cards
        let getHigherstCardForSuit suit = suit |> allForSuit |> Seq.sortBy (fun g-> g.Order) |> Seq.item 0
        let givePoints() = match Seq.length allExceptJacks with 
                            | 0 -> player.Hand |> Seq.item 0
                            | _ -> allExceptJacks |> Seq.sortBy (fun g-> g.Order) |> Seq.item 0
        let playJackForSuitOrWorstCard()= 
            let allInSuit=allForSuit (suitInPlay())
            match Seq.length allInSuit with
            |0-> match Seq.length allExceptJacks with 
                    | 0 -> lowestCardFromLowestSuit player.Hand
                    | _ -> lowestCardFromLowestSuit allExceptJacks
            | _->   let jackForCurrentSuit =playFirstJack allInSuit
                    match jackForCurrentSuit with
                    |None-> lowestCardFromLowestSuit allInSuit
                    |Some jack-> jack
        let underCutIfNoChoice bid = 
            let allNonThurupCardsValues = getallNonThurupCards bid
            match Seq.length allNonThurupCardsValues with
            |0 -> Cut (lowestCardFromLowestSuit player.Hand) // there are only thurupu cards, to undercut
            |_ -> match (lowestCardFromLowestSuit allNonThurupCardsValues).Points with // if all other cards are high and thurupus are low then undercut
                                    | 0 -> Play (lowestCardFromLowestSuit allNonThurupCardsValues)
                                    | _ -> match (lowestCardFromLowestSuit allNonThurupCardsValues).Points with
                                            | 0 -> Cut (lowestCardFromLowestSuit allNonThurupCardsValues)
                                            | _ -> Play (lowestCardFromLowestSuit allNonThurupCardsValues)
    //        let getTopNonThurupuByRank =allNonThurupCards |> Seq.sortBy (fun g->g.Points) |> Seq.nth 0 //rather than taking first iterate through and find suit with lowest count
    //        let currentSuit = sets.Head.Tricks.Head.PlayedCard.Suit
    //        let playerHasSuitInHand = player.Hand |> Seq.exists (fun g->g.Suit=currentSuit)    
    
        
        //check for thani
        if playerRoute = ThaniPlayer 
        then Play <| (playJackForSuitOrWorstCard())
        else
            let bid,isBidShown = GameAttributes.GetBid round.Bid
        
            let openThurupuOrPlay cutCard =
                match isBidShown with 
                |false -> ShowThurupu
                |true -> Play cutCard

            let openThurupuOrCut cutCard =
                match isBidShown with 
                |false -> ShowThurupu
                |true -> Cut cutCard

            match playerRoute with
                | FirstPlayerOfRound -> Play (PotentialThurupu player.Hand)
                | FirstPlayerOfRoundAndBidderAndAllThurupu -> Play (PotentialThurupu (bid |> getallNonThurupCards |> Seq.filter (fun g->g<>bid.Thurupu)))
                | FirstPlayerOfRoundAndBidder -> Play (PotentialThurupu (getallNonThurupCards bid))
                | FirstPlayerOfSet -> Play (firstJackOrLowestFromLowestSuit player.Hand)// any jack then play, else choose the suit with lowest number of cards and play lowest rank
                | FirstPlayerOfSetAndCanCut ->openThurupuOrCut (getLowestCardForSuit bid.Thurupu.Suit) //cut with lowest thurupu// flush thurupu if high cards in hand other wise choose above logic
                | FirstPlayerOfSetAndBidder ->Play (firstJackOrLowestFromLowestSuit (getallNonThurupCards bid))// same logic as two above 
                | FirstPlayerOfSetAndBidderAndAllThurupu ->openThurupuOrCut (firstJackOrLowestFromLowestSuit (getallNonThurupCards bid))// same as above
                | PlayerAbleToCutWithTeamWinning -> openThurupuOrCut (getLowestCardForSuit bid.Thurupu.Suit) // cut with lowest thurupu
                | PlayerAbleToUnderCutWithTeamWinning -> openThurupuOrCut (getLowestCardForSuit bid.Thurupu.Suit) // cut with lowest
                | PlayerAbleToOverCutWithTeamWinning -> openThurupuOrCut (getHigherstCardForSuit bid.Thurupu.Suit) // cut with highest
                | PlayerWithTeamWinning reason-> match reason with
                                                    | HasSuit ->Play (firstJackOrLowestFromSuit (suitInPlay()))//give points???
                                                    | NoThurupu ->Play (givePoints())// give points
                | PlayerAbleToCutWithOpponentWinning ->openThurupuOrCut (getLowestCardForSuit bid.Thurupu.Suit) // cut
                | PlayerAbleToUnderCutWithOpponentWinning -> underCutIfNoChoice bid
                | PlayerAbleToOverCutWithOpponentWinning ->  openThurupuOrCut (getHigherstCardForSuit bid.Thurupu.Suit) // cut
                | PlayerWithOpponentWinning reason-> match reason with 
                                                        | HasSuit ->Play (firstJackOrLowestFromSuit (suitInPlay()))
                                                        | NoThurupu ->Play (player.Hand|>lowestCardFromLowestSuit)
                | OpenThurupuButUnableToCutWithTeamWinning -> openThurupuOrPlay (givePoints())
                | OpenThurupuButUnableToCutdWithOpponentWinning ->openThurupuOrPlay (player.Hand|>lowestCardFromLowestSuit)
                | ThaniPlayer -> failwith "" //thani should have been handler earler during the routine
//
//            | _ -> Valid
//    let private ValidatePlay round suit rank playerId =
//        match (ValidateCurrentPlayer playerId round) with  
//        | Invalid x -> Invalid x
//        | Valid -> match CheckCheating suit rank round with
//                    | Some x -> Invalid (x|>toString)
//                    | None -> Valid
                                  
    let private getBidActions (playerBid:int) (round:RoundType) = 
        let currentPlayer = round|>GetCurrentPlayer
        let getNewBid bid = match bid.Value with
                                | prevBidValue when prevBidValue>=playerBid -> Pass
                                | _ -> PostBid bid
        let result =match round.Bid with
                        | ThaniBid _ 
                        | Bid _ 
                        | OpenedBid (_)-> failwith "thani??"
                        | ProcessingBid (_,bid) -> getNewBid bid
                        | WaitingForBid -> 
                            match playerBid with
                            | bid when bid<16 ->
                                {Value=16;Thurupu=currentPlayer.Thurupu();BidderId=currentPlayer.PlayerId} |> PostBid
                            | _-> {Value=playerBid;Thurupu=currentPlayer.Thurupu();BidderId=currentPlayer.PlayerId} |>PostBid
        match result with 
        | Pass -> Pass
        | PostBid bid -> match (ValidateBid bid.Value round.Bid) with
                                            | Valid -> result
                                            | _-> Pass
    let private getAiPlayerAction (round:RoundType)=
        let aiPlayer= round.Players |> Seq.tryFind ( fun g-> g.PlayerId=round.CurrentPlayerId && g.Ai)
        match aiPlayer with
        | None -> failwith "Player not an AI player"
        | Some x -> match round.Bid with
                    | WaitingForBid | ProcessingBid (_)-> PlayerActions.Bid (round |> getBidActions (int (x.Bid())))
                    | _ -> round |> getNext
    let private getPlayerAction (round:RoundType)=
        let player= round.Players |> Seq.tryFind ( fun g-> g.PlayerId=round.CurrentPlayerId)
        match player with
        | None -> failwith "Player not valid"
        | Some x -> match round.Bid with
                    | WaitingForBid | ProcessingBid (_)-> PlayerActions.Bid (round |> getBidActions (int (x.Bid())))
                    | _ -> round |> getNext
    let private processThaniWinner set (card:Card) round =
        let modifiedSet=match round.CurrentTrickIndex with 
                        | 0 -> {set with Winner= Some round.CurrentPlayerId}
                        | _ -> 
                                let startingTrickCard = (startingTrick round).PlayedCard
                                match startingTrickCard.Suit=card.Suit with
                                | true -> match card.Order < startingTrickCard.Order with 
                                            | true ->   { set with Winner=Some round.CurrentPlayerId }// game over
                                            | false -> set
                                | false -> set
                                

        match round.CurrentTrickIndex with
        | 0 -> match round.Sets with 
                |[]->{ round with Sets=[modifiedSet];}
                | _->{ round with Sets=modifiedSet::round.Sets}
        | _-> { round with Sets=modifiedSet::round.Sets.Tail;}
    let private processWinner set card round =
        let bid,isBidShown = GameAttributes.GetBid round.Bid
        let bidIsShown = GameAttributes.IsBidShown round.Bid
        let isCutCard=card.Suit=bid.Thurupu.Suit
        let isCutCardAndThurupuOpen= isCutCard && bidIsShown
        let modifiedSet= match round.CurrentTrickIndex with 
                            | 0 ->  match isCutCardAndThurupuOpen with
                                    | true -> { set with Winner=Some round.CurrentPlayerId; IsCut=Some true; CutBy=Some card;}
                                    | false -> { set with Winner=Some round.CurrentPlayerId }
                            | _ -> match isCutCardAndThurupuOpen with 
                                    | true -> match set.CutBy with
                                                | Some x -> match (isCutCard && (card.Order < x.Order)) with
                                                            | true-> {set with CutBy=Some card;Winner = Some round.CurrentPlayerId}
                                                            | false-> set
                                                | _ -> { set with IsCut=Some true; CutBy=Some card; Winner = Some round.CurrentPlayerId}
                                    | false -> match set.CutBy with 
                                                | Some x -> set
                                                | None ->   let prevWinner = set.Tricks |> Seq.find (fun g-> g.PlayerId=set.Winner.Value)
                                                            match (card.Suit=prevWinner.PlayedCard.Suit && prevWinner.PlayedCard.Order>card.Order) with
                                                            | true -> { set with Winner=Some round.CurrentPlayerId }
                                                            | false -> set
        match round.CurrentTrickIndex with
        | 0 -> match round.Sets with 
                |[]->{ round with Sets=[modifiedSet];}
                | _->{ round with Sets=modifiedSet::round.Sets}
        | _-> { round with Sets=modifiedSet::round.Sets.Tail;}
    let private processThaniSetAndTrickIndex round=
        let set = round.Sets.Head
        match round.CurrentTrickIndex with
                | 3 ->  let modifiedSet = { set with CompletedAt = Some DateTime.Now}
                        //let m = GetPlayer round System.Guid.Empty
                        {round with CurrentPlayerId=set.Winner.Value; CurrentTrickIndex=0;CurrentSetIndex=round.CurrentSetIndex+1; Sets=modifiedSet::round.Sets.Tail}
                | _ -> {round with CurrentTrickIndex=round.CurrentTrickIndex+1;CurrentPlayerId=(round|>GetNextPlayer).PlayerId}
    let private processSetAndTrickIndex round=
        let set = round.Sets.Head
        match round.CurrentTrickIndex with
                | 5 ->  let modifiedSet = { set with CompletedAt = Some DateTime.Now}

                        {round with CurrentPlayerId=set.Winner.Value; CurrentTrickIndex=0;CurrentSetIndex=round.CurrentSetIndex+1; Sets=modifiedSet::round.Sets.Tail;}
                | _ -> {round with CurrentTrickIndex=round.CurrentTrickIndex+1;CurrentPlayerId=(round|>GetNextPlayer).PlayerId}


    let private processKunukus (previousWinnerTeamDetails:int option) (round:RoundType)=
        let getPlayerTeamPoints (player:Player) = match player.TeamId with 
                                                    | 1 -> round.Score.Value.FirstTeamPoints
                                                    | 2 -> round.Score.Value.SecondTeamPoints
                                                    | _ -> failwith "invalid team id"
        // players Team Lost
        let playerInLoosingTeam (player:Player) = player.TeamId<>round.Score.Value.WinnerTeamId
        // players Team won
        let playerInWinningTeam (player:Player) = player.TeamId=round.Score.Value.WinnerTeamId
        // player is bidder and won
        let playerIsBidderAndWon (player:Player) = player.PlayerId=(round|>BidOwner).PlayerId && (playerInWinningTeam player)
        // if player is the bidder and won then -1 kunukus
        let processPlayerIsBidderAndWon (player:Player) = 
            match player|>playerIsBidderAndWon with
            | true when player.HasKunuku -> {player with Kunukus = player.Kunukus-1}
            | _ -> player
        // if players team has negative score then 1 kunukus 
        let processPlayerWithNegativeTeamScore (player:Player) = 
            match (getPlayerTeamPoints player)<0 && (playerInLoosingTeam player) with 
            | true -> {player with Kunukus = player.Kunukus+1}
            | false -> player
        // game after a kunuku was given 
        let kunukuWasJustGiven = round.Score.Value.FirstTeamPoints<0 || round.Score.Value.SecondTeamPoints<0 
        let prevRoundLooserBidAndWon = match previousWinnerTeamDetails with 
                                        | Some prevWinner
                                            //prev winner and current winner are different and bidder is winner
                                            when (prevWinner<>round.Score.Value.WinnerTeamId)&&((round|>BidOwner).TeamId=round.Score.Value.WinnerTeamId)
                                            -> true
                                        | _ -> false
        // if first game after a kunukus was given and team which lost prev round bid and won then remove one kunuku from each player
        let processGameAfterKunukuWasGiveAndPrevRoundLooserBidAndWon (player:Player)=
            match player.TeamId=round.Score.Value.WinnerTeamId && kunukuWasJustGiven && prevRoundLooserBidAndWon with
            |true -> {player with Kunukus=player.Kunukus-1}
            |false -> player

        let players = 
            round.Players 
            |> Seq.map (fun g-> 
                g   |>processPlayerIsBidderAndWon 
                    |>processPlayerWithNegativeTeamScore
                    |>processGameAfterKunukuWasGiveAndPrevRoundLooserBidAndWon ) 
            |> Seq.toList
        { round with Players=players }

    let private resetTeamPointsIfNegative (round:RoundType) =
        let roundScore = round.Score.Value;
        // if atleast one team has a negative socore then give then 6 each
        let score=match (roundScore.FirstTeamPoints<0||roundScore.SecondTeamPoints<0) with
                    | true ->  { roundScore with FirstTeamPoints=6; SecondTeamPoints=6;}
                    | false -> roundScore
        {round with Score=Some score}

    let private processTeamPoints score winner team1Points team2Points message = 
        match score with 
        // no previous score --> logic ok??
        | None ->  { WinnerTeamId=winner; FirstTeamPoints=6+team1Points; SecondTeamPoints=6+team2Points;Message=message;}
        // add or substract point 
        | Some x -> { WinnerTeamId=winner; FirstTeamPoints=x.FirstTeamPoints+team1Points; SecondTeamPoints=x.SecondTeamPoints+team2Points;Message=message;}
    
    let private getTeamPoints sets players = 
        sets 
        |> Seq.sumBy (fun g-> 
            match (players|> Seq.tryFind (fun h->g.Winner.IsSome && g.Winner.Value=h)) with
            | Some x -> g.Tricks |> Seq.sumBy (fun i->i.PlayedCard.Points)
            | None -> 0)

    let private teamWinnerRoute team1Players bidderId bidValue team1Points team2Points =
        match (team1Players|> Seq.exists (fun g->g=bidderId)) with
        | true -> match (team1Points>=bidValue) with
                    | true -> Team1BidAndWon
                    | false -> Team1BidAndLost
        | false -> match (team2Points>=bidValue) with 
                    | true -> Team2BidAndWon
                    | false -> Team2BidAndLost
    let private ifThaniEndedGetWinner round =
        let set = round.Sets.Head
        let trick = set.Tricks.Head
        match (round.CurrentSetIndex>5) with
        | true -> Some (round.Bid |> GetThaniPlayerId |> GetPlayer round)
        | false -> match set.Winner with
                    | Some x -> match x<>(round.Bid |> GetThaniPlayerId) with
                                    | true -> Some (x |> GetPlayer round)
                                    | false -> None
                    | _ -> None
    let private getThaniWinnerRoute (round:RoundType) (winner:Player)=
        let getWinnerRoute bidderTeamId winnerTeamId= 
            match bidderTeamId,winnerTeamId with
            | 1,1-> Team1BidAndWon
            | 1,2 ->Team1BidAndLost
            | 2,2 ->Team2BidAndWon
            | 2,1 ->Team2BidAndLost
            | _ -> failwith "unable to process thani winner"
        winner.TeamId |> getWinnerRoute ((round.Bid |> GetThaniPlayerId |> GetPlayer round)).TeamId
    let private processEndOfRound round (thaniWinner: Player option) = 
        let bidderId,bidValue= 
            match round.Bid with
            | ThaniBid userId -> userId,4
            | _ ->
                    let bid,_= GameAttributes.GetBid round.Bid
                    bid.BidderId,bid.Value
        let previousRoundWinnerTeamDetails = match round.Score with
                                                | None -> None 
                                                | Some x -> Some x.WinnerTeamId
        let getTeam1Players() = getTeamPlayers round.Players 1
        let getTeam2Players() = getTeamPlayers round.Players 2
        let getTeam1Points() = getTeam1Players() |> getTeamPoints round.Sets
        let getTeam2Points() = getTeam2Players() |> getTeamPoints round.Sets
        let getTeamBidType bid = match bid with 
                                    | ThaniBid _ -> Thani
                                    | _ -> match (bidValue) with 
                                            | 28 -> TwentyEight
                                            | value when value<20 -> Typical
                                            | _ -> Honors
        let getTeamWinnerRoute()= 
            match round.Bid with
            | ThaniBid _ -> getThaniWinnerRoute round thaniWinner.Value
            | _ -> teamWinnerRoute (getTeam1Players()) bidderId bidValue (getTeam1Points()) (getTeam2Points())
        let teamWinnerType =  (getTeamWinnerRoute(),round.Bid|>getTeamBidType)
        let score = 
            match teamWinnerType with 
            | (Team1BidAndWon,Thani) -> processTeamPoints round.Score 1 4 -4 (sprintf"Team 1 bid %i and won, +4 point" bidValue)
            | (Team1BidAndWon,Typical) -> processTeamPoints round.Score 1 1 -1 (sprintf"Team 1 bid %i and won, +1 point" bidValue)
            | (Team1BidAndWon,Honors) -> processTeamPoints round.Score 1 2 -2 (sprintf"Team 1 bid %i and won, +2 point" bidValue)
            | (Team1BidAndWon,TwentyEight) -> processTeamPoints round.Score 1 3 -3 (sprintf"Team 1 bid %i and won, +3 point" bidValue)
            | (Team1BidAndLost,Thani) -> processTeamPoints round.Score 2 -4 4 (sprintf"Team 1 bid %i and lost, -4 point" bidValue)
            | (Team1BidAndLost,Typical) -> processTeamPoints round.Score 2 -2 2 (sprintf"Team 1 bid %i and lost, -2 point" bidValue)
            | (Team1BidAndLost,Honors) -> processTeamPoints round.Score 2 -3 3 (sprintf"Team 1 bid %i and lost, -3 point" bidValue)
            | (Team1BidAndLost,TwentyEight) -> processTeamPoints round.Score 2 -4 4 (sprintf"Team 1 bid %i and lost, -4 point" bidValue)
            | (Team2BidAndWon,Thani) -> processTeamPoints round.Score 2 -4 4 (sprintf"Team 2 bid %i and won, +4 point" bidValue)
            | (Team2BidAndWon,Typical) -> processTeamPoints round.Score 2 -1 1 (sprintf"Team 2 bid %i and won, +1 point" bidValue)
            | (Team2BidAndWon,Honors) -> processTeamPoints round.Score 2 -2 2 (sprintf"Team 2 bid %i and won, +2 point" bidValue)
            | (Team2BidAndWon,TwentyEight) -> processTeamPoints round.Score 2 -3 3 (sprintf"Team 2 bid %i and won, +3 point" bidValue)
            | (Team2BidAndLost,Thani) -> processTeamPoints round.Score 1 4 -4 (sprintf"Team 2 bid %i and lost, -4 point" bidValue)
            | (Team2BidAndLost,Typical) -> processTeamPoints round.Score 1 2 -2 (sprintf"Team 2 bid %i and lost, -2 point" bidValue)
            | (Team2BidAndLost,Honors) -> processTeamPoints round.Score 1 3 -3 (sprintf"Team 2 bid %i and lost, -3 point" bidValue)
            | (Team2BidAndLost,TwentyEight) -> processTeamPoints round.Score 1 4 -4 (sprintf"Team 2 bid %i and lost, -4 point" bidValue)
        { round with Score=Some score} |>(processKunukus previousRoundWinnerTeamDetails)|>resetTeamPointsIfNegative |> (fun g-> {g with Status= Ended})
    let private processPlay card round =
        // create trick
        let trick = {PlayerId=round.CurrentPlayerId;PlayedCard=card;PlayedAt=System.DateTime.Now}
        // create sets or add trick to set
        let set = match round.CurrentTrickIndex with 
                    | 0 -> Set.DefaultSet trick
                    | _ ->  let thisSet=round.Sets.Head
                            { thisSet with Tricks=trick::thisSet.Tricks}
        let responseMessage = sprintf  "%A %A" card.Suit card.Rank
        (round|>GetCurrentPlayer).RemoveCardFromHand card
        let response = {  ResponseCode=ResponseCode.Success;Message=responseMessage;Created=System.DateTime.Now;CreatedBy=round.CurrentPlayerId;ResponseType=ResponseType.Public}
        match round.Bid with 
        | ThaniBid _ -> let processThaniRound = round |> processThaniWinner set card |> processThaniSetAndTrickIndex
                        match ifThaniEndedGetWinner processThaniRound with
                        | Some player ->processEndOfRound processThaniRound (Some player)
                        | None -> processThaniRound
        | _-> 
                    let processedRound = round |>processWinner set card |> processSetAndTrickIndex
                    match (processedRound.CurrentSetIndex>5) with
                    | true -> processEndOfRound processedRound None
                    | false -> processedRound
    let GetNextPlayerAction round =
        match round|>ValidateActiveRound with 
        | Invalid x -> failwith x
        | Valid ->round|>getPlayerAction

    let GetNextAiAction round =
        match round|>ValidateActiveRound with 
        | Invalid x -> failwith x
        | Valid ->
//            let aiAction = round|>GetAiPlayerAction
//            match aiAction,round.Bid with
//            | (Cut card,Bid bid) -> OpenAndCut card
//            | _ -> aiAction 
                round|>getAiPlayerAction
    let DoAiNextAction aiAction round=
        match round|>ValidateActiveRound with 
        | Invalid x -> failwith x
        | Valid ->
            match aiAction with
                | PlayerActions.Bid x -> 
                    match x with 
                            | Pass -> round|>PostPassAction
                            | PostBid bid-> round|>PostBidAction bid
                | Play card -> round |> processPlay card
                | Cut card -> round |> processPlay card
                | ShowThurupu -> round |> PostOpenThurupuAction
    let PostAction round playerId (action:UiPlayerAction) =
        match round|>ValidateActiveRound with 
        | Invalid x -> failwith x
        | Valid ->
            match (ValidateCurrentPlayer playerId round) with 
            | Invalid x -> failwith x
            | Valid ->
                match action with 
                            | UiBid bid -> match (validateCardInHand bid.Thurupu round) with
                                            | Invalid x -> failwith x
                                            | Valid -> match (ValidateBid bid.Value round.Bid) with
                                                                | Valid -> round|>PostBidAction bid
                                                                | Invalid msg -> failwith msg     
                            | UiPass -> match (ValidatePass playerId round) with
                                        | Valid -> round |> PostPassAction
                                        | Invalid msg -> failwith msg
                            | UiThani -> match (ValidateThaniCall playerId round) with
                                            | Valid -> round |> PostThaniAction
                                            | Invalid msg -> failwith msg
                            | UiShowThurupu -> match (ValidateShowThurupu playerId round Card.HasSuit) with 
                                                | Valid -> round |> PostOpenThurupuAction
                                                | Invalid msg -> failwith msg
                            | UiPlay x ->match CheckCheating x round with
                                            | Some x -> failwith (x|>GetCheatString)
                                            | None -> round |> processPlay x
        
