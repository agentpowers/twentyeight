﻿#load "Utilities.fs"
#load "Card.fs"
#load "GameAttributes.fs"
#load "Player.fs"
#load "Round.fs"
#load "ValidationAndCheating.fs"
#load "GameActions.fs"
#load "Game.fs"


open System
open TwentyEight.Game
open TwentyEight.Game.SinglePlayer
open TwentyEight.Game.Card
open TwentyEight.Game.Round
open TwentyEight.Game.GameAttributes

type UserId = 
        | Id of Guid
        member x.Value = let (Id v) = x in v

type Command = 
    | CreateGame of UserId*CreateGameArgs
    | RestartRound of UserId*NewRoundArgs
    | MoveToNextRound of UserId*NewRoundArgs
    | StartNewRound of UserId*NewRoundArgs
    | EndGame of UserId
    | Bid of UserId*Bid
    | Thani of UserId
    | Pass of UserId
    | OpenThurupu of UserId
    | Play of UserId*Card
    | PlayNext of UserId*PlayerActions

type Event = 
    | CreatedGame of UserId*CreateGameArgs
    | RestartedRound of UserId*NewRoundArgs
    | MovedToNextRound of UserId*NewRoundArgs
    | StartedNewRound of UserId*NewRoundArgs
    | EndedGame of UserId
    | BidPosted of UserId*Bid
    | ThaniPosted of UserId
    | PassPosted of UserId
    | OpenThurupuPosted of UserId
    | Played of UserId*Card
    | PlayedNext of UserId*PlayerActions

let assignRoundToGame game round=
    {game with CurrRound=round}

let zero =
    Game.GetEmptyGame()

let apply (item:Game)= function
    | CreatedGame (userId,args) -> args|>Game.NewGame
    | RestartedRound (userId,args) -> args |> item.RestartRound
    | MovedToNextRound (userId,args) -> args|>item.MoveToNextRound
    | StartedNewRound (userId,args) -> args |> item.NewRound 
    | EndedGame userid-> item.EndGame()
    | BidPosted (userId,bid) -> bid |> UiPlayerAction.UiBid |> PostAction item.CurrRound userId.Value |> assignRoundToGame item
    | ThaniPosted userId-> UiPlayerAction.UiThani |> PostAction item.CurrRound userId.Value|> assignRoundToGame item
    | PassPosted  userId-> UiPlayerAction.UiPass |> PostAction item.CurrRound userId.Value|> assignRoundToGame item
    | OpenThurupuPosted userId-> UiPlayerAction.UiShowThurupu |> PostAction item.CurrRound userId.Value|> assignRoundToGame item
    | Played (userId,card)-> card |> UiPlayerAction.UiPlay |> PostAction item.CurrRound userId.Value|> assignRoundToGame item
    | PlayedNext (userId,playerAction) -> item.CurrRound |> DoAiNextAction playerAction |> assignRoundToGame item

let exec item = 
    let apply event = 
        let newItem = apply item event
        newItem,event
    function
    | CreateGame (userId,args)          -> (userId,args)  |> CreatedGame |> apply                    
    | RestartRound (userId,args)        -> (userId,args)  |> RestartedRound |> apply        
    | MoveToNextRound (userId,args)     -> (userId,args)  |> MovedToNextRound |> apply 
    | StartNewRound (userId,args)       -> (userId,args)  |> StartedNewRound |> apply 
    | EndGame userId                    -> userId |> EndedGame |> apply     
    | Bid (userId,bid)                  -> (userId,bid)|> BidPosted |> apply
    | Thani userId                      -> userId |> ThaniPosted |> apply       
    | Pass userId                       -> userId |> PassPosted |> apply        
    | OpenThurupu userId                -> userId |> OpenThurupuPosted |> apply 
    | Play (userId,card)                -> (userId,card) |> Played |> apply   
    | PlayNext (userId,playerAction)    -> (userId,playerAction) |> PlayedNext |> apply

let GetUserIdFromCommand command = 
    match command with
    | CreateGame (userId,args)          -> userId.Value                  
    | RestartRound (userId,args)        -> userId.Value            
    | MoveToNextRound (userId,args)     -> userId.Value       
    | StartNewRound (userId,args)       -> userId.Value
    | EndGame userId                    -> userId.Value    
    | Bid (userId,bid)                  -> userId.Value
    | Thani userId                      -> userId.Value      
    | Pass userId                       -> userId.Value     
    | OpenThurupu userId                -> userId.Value 
    | Play (userId,card)                -> userId.Value   
    | PlayNext (userId,playerAction)    -> userId.Value

let playerId=Guid.NewGuid()
let id=Guid.NewGuid()
let gameId= Guid.NewGuid()
let players =TwentyEight.Game.Player.PopulatePlayers ["A";"X";"B";"Y";"C";"Z"] playerId
let newGameArgs = {GameId=gameId;RoundId=id;UserId=playerId;Team1Name="Team1";Team2Name="Team2"; Players=players; CreatedByUser=playerId;CreatedTime=DateTime.Now}
let userId = (playerId|>UserId.Id)
let newGame,newGameEvent =exec (Game.GetEmptyGame()) ((userId,newGameArgs)|>Command.CreateGame)
let newRoundArgs = {RoundStartingPlayerId=playerId;RoundStartingPlayerIndex=0;RoundPlayers=players|> Round.ShuffleCardsToPlayers}
let newRound,newRoundEvent = exec newGame ((userId,newRoundArgs)|>Command.StartNewRound)
let card = (newRound.CurrRound.Players|> List.head).Hand |> List.head
let bid ={Value=16;Thurupu=card;BidderId=playerId}
let command = (userId,bid)|>Command.Bid
let withFirstBid,firstBidEvent = exec newGame command