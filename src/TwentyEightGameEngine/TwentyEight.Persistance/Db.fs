﻿namespace TwentyEight.Persistance

open System.Data
module Db=
    open System
    open Npgsql
    open Dapper
    open Microsoft.FSharp.Reflection

    let mutable ConnectionString = 
        #if INTERACTIVE
        "server=localhost; User Id=twentyeight; Password=welcome; database=twentyeight"
        //"Host=192.168.161.129; Port=5432; User Id=twentyeight; Password=blah; database=twentyeight"
        #endif
        #if !INTERACTIVE
        ""
        //System.Configuration.ConfigurationManager.ConnectionStrings.["PostgresDotNet"].ConnectionString
        #endif

    let startedAt  = DateTime.Now

    let GetUnionCaseName (x:'a) = 
        match FSharpValue.GetUnionFields(x, typeof<'a>) with
        | case, _ -> case.Name 

    let connection() = new NpgsqlConnection(ConnectionString)

    let getOpenConnection() = 
        let conn =connection()
        conn.Open()
        conn
    (*
        delete  from TelegramGame;
        delete  from GameStats;
        delete  from Aggregate;
        delete  from Events;
        delete  from Snapshots;
    *)
    let telegramTableSql = 
        @"CREATE TABLE IF NOT EXISTS TelegramGame (
            Id serial primary key not null,
            TelegramId integer not null,
            Data json,
            GameId uuid not null
        );" 
    [<CLIMutable>]
    type TelegramGame = {Id:int;GameId:Guid;TelegramId:int64;Data:string}

    let gameStatTableSql = 
        @"CREATE TABLE IF NOT EXISTS GameStats (
            Id serial primary key not null,
            GameId uuid not null,
            Created timestamp default current_timestamp
        );" 
    [<CLIMutable>]
    type GameStat = {Id:int;GameId:Guid;Created:DateTime}

    let newAggregateTableSql= 
        @"CREATE TABLE IF NOT EXISTS Aggregate (
            AggregateId UUID primary key not null,
            Version integer not null DEFAULT 0,
            Type varchar(255) not null,
            Created timestamp default current_timestamp
        );" 
    [<CLIMutable>]
    type Aggregate = {AggregateId:Guid;Version:int;Type:string;Created:DateTime}

    let newEventsTableSql= 
        @"CREATE TABLE IF NOT EXISTS Events (
            Sequence BIGSERIAL primary key not null,
            AggregateId UUID not null,
            Version integer not null DEFAULT 0,
            Type varchar(255) not null,
            Data json,
            Created timestamp default current_timestamp
        );" 
    [<CLIMutable>]
    type Event = {Sequence:int64;AggregateId:Guid;Version:int;Type:string;Data:string;Created:DateTime}

    let newSnapshotsTabelSql= 
        @"CREATE TABLE IF NOT EXISTS Snapshots (
            Sequence BIGSERIAL primary key not null,
            AggregateId UUID not null,
            Version integer not null DEFAULT 0,
            LastEventSequence bigint not null,
            Data json,
            Created timestamp default current_timestamp
        );"
    [<CLIMutable>]
    type Snapshot = {Sequence:int64;AggregateId:Guid;Version:int;LastEventSequence:int64;Data:string;Created:DateTime}

    let insertGameStatSql =
        @"insert into GameStats(GameId) values (@GameId)"

    let insertTelegramGameSql =
        @"insert into TelegramGame(GameId,TelegramId,Data) values (@GameId,@TelegramId,@Data)"

    let updateTelegramGameSql =
        @"update TelegramGame set Data = @Data where GameId=@GameId"

    let getTelegramSql =
        "select * from TelegramGame where TelegramId = @TelegramId"

    let insertAggregateSql=
        "insert into Aggregate(AggregateId,Version,Type) values (@AggregateId,@Version,@Type)"

    let insertSnapShotSql=
        "insert into Snapshots(AggregateId,Version,LastEventSequence,Data) values (@AggregateId,@Version,@LastEventSequence,@Data)"

    let insertGameEventSql=
        "insert into Events(AggregateId,Version,Type,Data) values (@AggregateId,@Version,@Type,@Data)"

    let getAggregateSql =
        "select * from Aggregate where AggregateId=@AggregateId and Type=@Type"

    let getAggregateByTypeSql =
        "select * from Aggregate where Type=@Type"

    let getSnapshotSql =
        "select * from Snapshots where AggregateId=@AggregateId order by Sequence desc limit 1"

    let getSnapshotLastEventSequenceSql =
        "select LastEventSequence from Snapshots where AggregateId=@AggregateId order by Sequence desc limit 1"

    let getEventsSql =
        "select * from Events where AggregateId=@AggregateId and Sequence > @LastEventSequence order by Sequence"

    let getLastEventsSql =
        "select * from Events where AggregateId=@AggregateId order by Sequence desc limit 1"  

    let lastNewRoundEventSql= 
        "select * from Events where AggregateId=@AggregateId and Type in ('CreatedGame','MovedToNextRound','RestartedRound','StartedNewRound') order by Sequence desc limit 1"

    let saveGameStats id (conn:NpgsqlConnection)=
        conn.Execute(insertGameStatSql,{GameStat.Id=0;GameId=id;Created=DateTime.Now})

    let getTelegramGame (telegramId:int64) (conn:NpgsqlConnection) =
        conn.Query<TelegramGame>(getTelegramSql,{Id=0;GameId=Guid.Empty;TelegramId=telegramId;Data=""})|>Seq.tryHead

    let objFunction (o:obj) = o

    //let getAggregateAsync (id:string) (conn:NpgsqlConnection)= 
    //    async{
    //        let! snapShots = conn.QueryAsync<Snapshot>(id|>getSnapshotSql) |> Async.AwaitTask
    //        let snapShot = snapShots |> Seq.head
    //        let! events = conn.QueryAsync<Event>(id|>getEventsSql,snapShot) |> Async.AwaitTask
    //        let snapShotObj = fromJson<Game>(snapShot.Data)
    //        let eventObjs =  events |> Seq.map (fun i -> fromJson<GameItem.Event>(i.Data))
    //        return snapShotObj,eventObjs
    //    }

    let tryGetEvents (snapShot:Snapshot) (conn:NpgsqlConnection)= 
        conn.Query<Event>(getEventsSql,snapShot) |> Seq.toArray

    let tryGetSnapShotsAndEvents (aggregate:Aggregate) (conn:NpgsqlConnection)= 
        let someSnapShot = conn.Query<Snapshot>(getSnapshotSql,aggregate)|>Seq.tryHead
        match someSnapShot with
        |None -> None, conn |> tryGetEvents {Snapshot.Sequence=0L;AggregateId=aggregate.AggregateId;Version=0;LastEventSequence=0L;Data="";Created=startedAt} 
        |Some snapShot -> 
            Some snapShot,conn |> tryGetEvents snapShot

    let getSnapshotLastEventSequence (aggregate:Aggregate) (conn:NpgsqlConnection) =
         conn.Query<int64>(getSnapshotLastEventSequenceSql,aggregate) |> Seq.tryHead

    let getAggregate (id:Guid) typeName (conn:NpgsqlConnection)=
         conn.Query<Aggregate>(getAggregateSql,{Aggregate.AggregateId=id;Version=0;Type=typeName;Created=DateTime.Now})|>Seq.tryHead

    let getAggregateByTypeName (typeName:string) (conn:NpgsqlConnection)=
         conn.Query<Aggregate>(getAggregateByTypeSql,{Aggregate.AggregateId=Guid.Empty;Version=0;Type=typeName;Created=startedAt})|>Seq.tryHead

    let getAggregatesByTypeName (typeName:string) (conn:NpgsqlConnection)=
         conn.Query<Aggregate>(getAggregateByTypeSql,{Aggregate.AggregateId=Guid.Empty;Version=0;Type=typeName;Created=startedAt})

    let getLastEvent (aggregate:Aggregate) (conn:NpgsqlConnection) =
        conn.Query<Event>(getLastEventsSql,aggregate)|>Seq.head

    let tryGetLastEvent (aggregate:Aggregate) (conn:NpgsqlConnection) =
        conn.Query<Event>(getLastEventsSql,aggregate)|>Seq.tryHead

    let getLastNewRoundEvent ((aggregate:Aggregate)) (conn:NpgsqlConnection) =
        conn.Query<Event>(lastNewRoundEventSql,aggregate)|>Seq.head

    let saveAggregate (aggregate:Aggregate) (conn:NpgsqlConnection)=
        use command = new NpgsqlCommand(insertAggregateSql,conn)
        command.Parameters.AddWithValue("@AggregateId",aggregate.AggregateId) |>ignore
        command.Parameters.AddWithValue("@Version",aggregate.Version) |>ignore
        command.Parameters.AddWithValue("@Type",aggregate.Type) |>ignore
        command.ExecuteScalar()

    let saveSnapshot (snapshot:Snapshot) (conn:NpgsqlConnection)=
        use command = new NpgsqlCommand(insertSnapShotSql,conn)
        command.Parameters.AddWithValue("@AggregateId",snapshot.AggregateId) |>ignore
        command.Parameters.AddWithValue("@Version",snapshot.Version) |>ignore
        command.Parameters.AddWithValue("@LastEventSequence",snapshot.LastEventSequence) |>ignore
        command.Parameters.AddWithValue("@Data",NpgsqlTypes.NpgsqlDbType.Json,snapshot.Data) |>ignore
        command.ExecuteScalar()

    let saveEvent (event:Event) (conn:NpgsqlConnection)=
        use command = new NpgsqlCommand(insertGameEventSql,conn)
        command.Parameters.AddWithValue("@AggregateId",event.AggregateId) |>ignore
        command.Parameters.AddWithValue("@Version",event.Version) |>ignore
        command.Parameters.AddWithValue("@Type",event.Type) |>ignore
        command.Parameters.AddWithValue("@Data",NpgsqlTypes.NpgsqlDbType.Json,event.Data) |>ignore
        command.ExecuteScalar()

    let saveTelegram gameId telegramid eventJson (conn:NpgsqlConnection)=
        use command = new NpgsqlCommand(insertTelegramGameSql,conn)
        command.Parameters.AddWithValue("@GameId",gameId) |>ignore
        command.Parameters.AddWithValue("@TelegramId",telegramid) |> ignore
        command.Parameters.AddWithValue("@Data",NpgsqlTypes.NpgsqlDbType.Json,eventJson) |>ignore
        command.ExecuteScalar()

    let updateTelegram telegramId jsonData (conn:NpgsqlConnection)=
        use command = new NpgsqlCommand(updateTelegramGameSql,conn)
        command.Parameters.AddWithValue("@TelegramId",telegramId) |> ignore
        command.Parameters.AddWithValue("@Data",NpgsqlTypes.NpgsqlDbType.Json,jsonData) |>ignore
        command.ExecuteScalar()

    let init(connectionString)=
        ConnectionString <- connectionString
        use conn = getOpenConnection()
        conn.Execute(telegramTableSql) |> ignore
        conn.Execute(gameStatTableSql) |> ignore
        conn.Execute(newAggregateTableSql) |> ignore
        conn.Execute(newEventsTableSql) |> ignore
        conn.Execute(newSnapshotsTabelSql) |> ignore
        conn.Close() |> ignore




