﻿namespace TwentyEight.Core

module Global=
    type TelegramUser ={
            TelegramId:int64
            GameUserId:System.Guid
            TelegramName:string
        }

    type WebUser = {
        UserId:System.Guid
        Name:string
    }

    type ErrorType = 
        | ``Unable to match telegram id in game session``
    
    let GetErrorMsg errorType =
        match errorType with
        |``Unable to match telegram id in game session`` -> "Oops something went wrong \ud83d\ude14 Error Code : 1000"